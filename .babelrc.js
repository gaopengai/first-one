const plugins = [
  [
    "import",
    {
      libraryName: "antd-mobile",
      style: "css",
    },
  ],
]; // `style: true` 会加载 less 文件];
if (process.env.NODE_ENV === "production") {
  plugins.push("transform-remove-console");
}
module.exports = {
  presets: ["react-app"],
  plugins,
};
