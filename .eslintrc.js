module.exports = {
  extends: [
    "react-app",
    "react-app/jest",
    // "eslint-config-alloy/react", //针对项目技术栈配置,可选 React、Vue（现已支持 Vue 3.0）、TypeScript 等；
  ],
  root: true,
  parser: "babel-eslint",
  parserOptions: {
    env: { es6: true },
    // 设置为 script (默认) 或 module（如果你的代码是 ECMAScript 模块)
    sourceType: "module",
    // 这是个对象，表示你想使用的额外的语言特性,所有选项默认都是 false
    ecmaFeatures: {
      // 是否启用JSX
      jsx: true,
    },
  },
  env: {
    browser: true, //浏览器全局变量
    node: true, //Node.js全局变量和作用域；
    es6: true,
    jest: true,
  },
  // react.eslintrc.js 里就把spyOnDev、spyOnProd等变量挂在了global下作为全局变量：
  globals: {
    spyOnDev: true, //true、writeable、writable 这 3 个是等价的，表示变量可读可写；
    spyOnProd: true,
  },
  // rules,extends检查JS语法,   引入插件的目的就是为了增强 ESLint 的检查能力和范围。
  plugins: ["react", "import", "jsx-a11y", "react-hooks"],

  rules: {
    // off 或 0：关闭对该规则的校验；
    // warn 或 1：启用规则，不满足时抛出警告，且不会退出编译进程；
    // error 或 2：启用规则，不满足时抛出错误，且会退出编译进程；
    "no-debugger": process.env.REACT_APP_ENV === "production" ? "error" : "off",
    //   eqeqeq: "off",
    //   "jsx-a11y/click-events-have-key-events": ["off"],
    //   "no-multi-assign": ["off"],
    //   "class-methods-use-this": ["off"],
    //   "react-hooks/rules-of-hooks": "error", // 检查 Hook 的规则
    //   "react-hooks/exhaustive-deps": "warn", // 检查 effect 的依赖
  },
};
