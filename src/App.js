import { useState, useEffect } from "react";

function App() {
  let data = [];
  for (let i = 0; i < 100; i++) {
    data[i] = "今日头条99" + i;
  }
  const [state, setState] = useState({
    newdata: [],
    page: 1,
    limit: 5,
  });
  const handleclick = () => {
    setState({
      ...state,
      page: state.page + 1,
    });
  };
  useEffect(() => {
    const start = (state.page - 1) * state.limit;
    setState({
      ...state,
      newdata: data.slice(start, state.page * state.limit),
    });
  }, [state.page]);

  return (
    <div>
     
      <ul>
        {state.newdata.map((item) => {
          return <li key={item}>{item}</li>;
        })}
      </ul>
      <button onClick={handleclick}>下一页</button>
    </div>
  );
}

export default App;
