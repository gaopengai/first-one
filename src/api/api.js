import axios from "@/utils/HttpTool";
// 首页热门楼盘
export const hotfloor = () => axios.get("/yapi/customIndex.html");
// 首页搜索
export const getsearch = () => axios.get("/yapi/searchList");
// 二手房数据
export const geterdata = () => axios.get("/yapi/erinfoData.html");
// 轮播图
export const swiper = () => axios.get("/api/home/swiper");
// 登录
export const _Login = (params) => axios.post("/api/admin/login", params);
// 发布
export const getpublish = () => axios.get("/api/houses/params");
// 我的发布
export const issuehouse = () => axios.get("/api/admin/houses");

//获取管里员信息

export const admin = () => axios.get("/api/admin/users");
// 我的发布
// export const issuehouse = () => axios.get("/api/user/houses");

//获取城市列表
export const getcity = () =>
  axios.get("/api/area/city", {
    params: {
      level: "1",
    },
  });

// 获取子集城市
export const getarea = (params) => axios.get("/api/area", { params });

/************************ about issue module interfaces  ***************************************************************** */
//发布模块总数据
export const getAllIssueData = (params) =>
  axios.get("/yapi/release/getReleaseFieldValue.html", { params });
//经纪人活跃榜
export const agentMemberList = (params) =>
  axios.get("/yapi/agent/agentMemberList", { params });
//经纪人优选门店
export const agentStoreList = (params) =>
  axios.get("/yapi/agentCompany/agentStoreList", { params });
//经纪人实力榜
export const companyList = (params) =>
  axios.get("/yapi/agentCompany/companyList", { params });
//经纪人详情
export const agentInfo = (params) =>
  axios.get("/yapi/member/agentInfo", { params });
//经纪人house
export const agentDetail = (params) =>
  axios.get("/yapi/member/agentDetail.html", { params });
//楼市圈数据获取
export const building_circle = (params) =>
  axios.get("/yapi/building_circle/lists", { params });
//消息列表获取
export const chatFriends=(params)=>
axios.get("/yapi/im/chatFriends.html",{params})
//聊天详情获取
export const contactDetails=(params)=>
axios.get("/yapi/im/contactDetails.html",{params})
//聊天内容获取
export const chatLog=(params)=>
axios.get("/yapi/im/chatLog.html",{params})

//查找小区模糊搜索接口
export const getFuzzySearchList = (params) =>
  axios.get("/yapi/house/searchCommunity.html", { params });

/******************************************************************************************************** */
//咨询数据
export const getconult=()=>axios.get('/yapi/consultdata') 
//咨询数据详情
export const getnewsdetail=()=>axios.get('/yapi/newsdetail') 
//求租
export const getwanted=()=>axios.get('/yapi/wanteddata')
//求租区域
export const gethouseCondition=()=>axios.get('/yapi/houseCondition')
