import { useRef, useEffect, useState } from "react";
import { Toast } from "antd-mobile";
import style from "./amap.module.scss";
const Map = ({ input, changemask }) => {
  const map = useRef(null);
  let mMap = useRef(null);
  const [lng, setlng] = useState(116.4);
  const [lat, setlat] = useState(39.9);
  const [posi, setposi] = useState("北京市");
  const initMap = () => {
    mMap.current = new window.AMap.Map(map.current, {
      resizeEnable: true,
      zoom: 16,
      viewMode: "3D",
      touchZoomCenter: 1,
      doubleClickZoom: true,
      center: [lng, lat],
    });
    mMap.current.plugin(["AMap.Geolocation"], function () {
      var geolocation = new window.AMap.Geolocation({
        // 是否使用高精度定位，默认：true
        enableHighAccuracy: true,
        // 设置定位超时时间，默认：无穷大
        timeout: 10000,
        // 定位按钮的停靠位置的偏移量，默认：Pixel(10, 20)
        buttonOffset: new window.AMap.Pixel(10, 20),
        //  定位成功后调整地图视野范围使定位位置及精度范围视野内可见，默认：false
        zoomToAccuracy: true,
        //  定位按钮的排放位置,  RB表示右下
        buttonPosition: "RB",
        showButton: true,
      });
      geolocation.getCurrentPosition();
      window.AMap.event.addListener(geolocation, "complete", onComplete);
      window.AMap.event.addListener(geolocation, "error", onError);
      function onComplete(data) {
        // data是具体的定位信息
        changemask && changemask();
        Toast.success("定位成功", 1);
        setlng(data.position.lng);
        setlat(data.position.lat);
        setposi(data.formattedAddress);
      }

      function onError(data) {
        changemask && changemask();
        Toast.fail("定位出错", 1);
        // 定位出错
      }
    });
    mMap.current.plugin(["AMap.Autocomplete", "AMap.PlaceSearch"], function () {
      var autoOptions = {
        city: "全国", //城市，默认全国
        input: input.current, //使用联想输入的input的id
      };
      let autocomplete = new window.AMap.Autocomplete(autoOptions);
      var placeSearch = new window.AMap.PlaceSearch({
        city: "北京",
        map: mMap.current,
      });
      window.AMap.event.addListener(autocomplete, "select", function (e) {
        //TODO 针对选中的poi实现自己的功能
        placeSearch.setCity(e.poi.adcode);
        placeSearch.search(e.poi.name);
        setposi(e.poi.name);
      });
    });
  };
  useEffect(() => {
    initMap();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [lng, lat]);
  return (
    <div className={style.container}>
      <div className={style.map} ref={map}></div>
      <div className={style.posi}>当前位置: {posi}</div>
    </div>
  );
};

export default Map;
