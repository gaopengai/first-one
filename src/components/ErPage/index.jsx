import { useEffect } from "react";
import { connect } from "react-redux";
import { getErAll } from "@/store/action/action";
import style from "./index.module.scss";

const ErPage = (props) => {
  useEffect(() => {
    props.getErList();
  }, []);
  return (
    <div className={style.erall}>
      <h2>二手房</h2>
      {props.list.length &&
        props.list.map((item) => {
          return (
            <dl key={item.id}>
              <dt>
                <img src={item.img} alt="" />
              </dt>
              <dd>
                <p className={style.erall_tit}>{item.title}</p>
                <p className={style.erall_label}>
                  {item.label.map((itm) => {
                    return (
                      <span
                        className={style.erall_name}
                        key={itm.name}
                        style={{ color: itm.color }}
                      >
                        {itm.name}
                      </span>
                    );
                  })}
                </p>
                <ul>
                  <li>
                    {item.fangjia}
                    <span>万</span>
                  </li>
                  <li>{item.danjia}元㎡</li>
                  <li>{item.begintime}</li>
                </ul>
              </dd>
            </dl>
          );
        })}
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    list: state.erlistdata,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    getErList() {
      dispatch(getErAll());
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(ErPage);
