import { Component } from "react";
import * as Sentry from "@sentry/react";
import { Integrations } from "@sentry/tracing";

Sentry.init({
  dsn: "https://60d6e0035e014851afa623e6b2c67445@o959912.ingest.sentry.io/5908820",
  integrations: [new Integrations.BrowserTracing()],
  environment: process.env.REACT_APP_ENV,
  tracesSampleRate: 1.0,
});

class FancyComponent extends Component {
  componentDidCatch(error) {
    Sentry.captureException(error);
  }
  render() {
    return this.props.children;
  }
}
export default FancyComponent;
