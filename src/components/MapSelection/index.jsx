import { Icon, Range } from "antd-mobile";
import style from "./mapselection.module.scss";
import classNames from "classnames";
import { useState, useRef, useEffect } from "react";
import { withRouter } from "react-router-dom";
const MapSelection = (props) => {
  /* eslint arrow-body-style: 0 */
  // tab筛选区ref
  const selectable = useRef(null);
  // 筛选内容区高度
  const [bottom, setbottom] = useState("0px");
  // 区域左侧高亮
  const [leftind, setleftind] = useState(0);
  // 区域右侧高亮
  const [rightind, setrightind] = useState(0);
  // tab筛选区高亮
  const [needind, setneedind] = useState(-1);
  // 买房选型高亮
  const [status, setstatus] = useState(0);
  // 户型高亮
  const [type, settype] = useState(0);
  // 验证高亮
  const [verify, setverify] = useState(0);
  // 朝向高亮
  const [ori, setori] = useState(-1);
  // 筛选状态
  const [need, setneed] = useState("");
  // 价格区间
  const pricelist = [
    "不限",
    "≤10万",
    "20万-30万",
    "30万-50万",
    "50万-100万",
    "≥1000万",
  ];
  // 户型
  const housetype = {
    status: ["不限", "新房", "二手房"],
    type: ["不限", "1室", "2室", "3室", "3室+"],
  };
  // 筛选
  const filt = {
    verify: ["不限", "安选"],
    ori: ["东", "西", "南", "北", "南北"],
  };
  // 距离
  const dastance = [
    "不限",
    "1000米内",
    "2000米内",
    "3000米内",
    "4000米内",
    "5000米内",
    "6000米内",
  ];
  // 按钮
  const [btn, setbtn] = useState(false);
  // 排序
  const sortlist = ["默认排序", "单价由低到高", "单价由高到低"];
  // 价格高亮
  const [prind, setprind] = useState(0);
  // 排序ref
  const sort = useRef(null);
  // 筛选参数
  const [state, setstate] = useState({
    pri: "￥100万 - ￥1000万",
    city: "北京",
    area: "朝阳",
    house: "",
    room: "",
    // distance: "",
    verify: "",
    origin: "",
    // sort: "",
  });
  const changeneed = (val, i) => {
    props.ontitle && props.ontitle(val.title);
    setneedind(i);
    if (val.title === "距离" && props.changelayer) {
      props.changelayer(true);
    }
    setneed(val.title);
  };
  const changecity = (val, i) => {
    props.onid && props.onid(val.value);
    setleftind(i);
    setrightind(0);
    setstate({
      ...state,
      city: val.label,
    });
  };
  const getareainfo = (val, i) => {
    setrightind(i);
    setstate({
      ...state,
      area: val.label,
    });
  };
  const changepri = (i, val) => {
    setprind(i);
    setstate({
      ...state,
      pri: val,
    });
  };

  const log = (name) => {
    return (value) => {
      setstate({
        ...state,
        pri: `￥${value[0]}万 - ￥${value[1]}万`,
      });
    };
  };
  const changed = (name) => {
    return (value) => {
      setstate({
        ...state,
        pri: `￥${value[0]}万 - ￥${value[1]}万`,
      });
    };
  };
  const changestatus = (ite, i) => {
    setstatus(i);
    setstate({
      ...state,
      house: ite,
    });
  };

  const changetype = (ite, i) => {
    settype(i);
    setstate({
      ...state,
      room: ite,
    });
  };

  const changedastance = (ite) => {
    setneed("");
    props.changelayer(false);
    console.log(ite);
  };
  const changeverify = (ite, i) => {
    setverify(i);
    setstate({
      ...state,
      verify: ite,
    });
  };
  const changeori = (ite, i) => {
    setori(i);
    setstate({
      ...state,
      origin: ite,
    });
  };
  const changesort = (ite) => {
    props.changelayer && props.changelayer(false);
    setneed("");
    console.log(ite);
  };
  const rise = () => {
    if (props.history.location.pathname !== "/filrate") {
      setneedind(-1);
      props.changelayer && props.changelayer(true);
      setneed("排序");
    }
  };
  useEffect(() => {
    let h = selectable.current.offsetHeight;
    let top = selectable.current.offsetTop;
    let b = window.innerHeight - h - top - 44;
    setbottom(b);
    if (props.history.location.pathname === "/filrate") {
      setneedind(0);
      setbtn(true);
      setneed("区域");
    }
  }, []);

  const submit = () => {
    console.log(state);
  };
  return (
    <div className={style.mapnav2} ref={selectable}>
      <div className={style.mapnavcon}>
        {props.list.map((ite, i) => {
          return (
            <div
              className={classNames(style.setion)}
              key={ite.title}
              onClick={() => changeneed(ite, i)}
            >
              <span
                className={classNames(
                  needind === i ? style.active : null,
                  ite.title
                )}
              >
                {ite.title}
                <Icon type={ite.icon} className={ite.title}></Icon>
              </span>
            </div>
          );
        })}
        <div className={classNames(style.rise, style.setion)} onClick={rise}>
          ||
        </div>
      </div>

      <div
        className={style.selectarea}
        style={{
          height: bottom + "px",
          display: need === "区域" ? "flex" : "none",
        }}
      >
        <ul className={style.navleft}>
          {props.city.map((ite, i) => {
            return (
              <li
                className={leftind === i ? style.active : null}
                key={ite.value}
                onClick={() => changecity(ite, i)}
              >
                {ite.label}
              </li>
            );
          })}
        </ul>
        <ul className={style.navright}>
          {props.area?.length > 0 &&
            props.area.map((ite, i) => {
              return (
                <li
                  key={ite.value}
                  className={rightind === i ? style.active : null}
                  onClick={() => getareainfo(ite, i)}
                >
                  {ite.label}
                </li>
              );
            })}
        </ul>
      </div>

      <div
        className={style.price}
        style={{
          height: bottom + "px",
          display: need === "价格" || need === "租金" ? "flex" : "none",
        }}
      >
        <ul className={style.priceselect}>
          {pricelist.map((ite, i) => {
            return (
              <li
                className={prind === i ? style.active : null}
                onClick={() => changepri(i, ite)}
                key={i}
              >
                {ite}
              </li>
            );
          })}
        </ul>
        <div className={style.range}>
          <p className={style.pri}>{state.pri}</p>
          <Range
            style={{ marginLeft: 30, marginRight: 30 }}
            min={10}
            max={1000}
            defaultValue={[100, 1000]}
            onChange={changed()}
            onAfterChange={log()}
          />
        </div>
      </div>

      <div
        className={style.housetype}
        style={{
          height: bottom + "px",
          display: need === "户型" ? "flex" : "none",
        }}
      >
        <div className={style.ht}>
          <h3>买房选型</h3>
          <div>
            {housetype.status.map((ite, i) => {
              return (
                <span
                  className={status === i ? style.active : null}
                  key={i}
                  onClick={() => changestatus(ite, i)}
                >
                  {ite}
                </span>
              );
            })}
          </div>
        </div>

        <div className={style.type}>
          <h3>户型</h3>
          <div>
            {housetype.type.map((ite, i) => {
              return (
                <span
                  className={type === i ? style.active : null}
                  onClick={() => changetype(ite, i)}
                  key={i}
                >
                  {ite}
                </span>
              );
            })}
          </div>
        </div>
      </div>

      <div
        className={style.filters}
        style={{
          height: bottom + "px",
          display: need === "筛选" ? "flex" : "none",
        }}
      >
        <div className={style.verify}>
          <h3>验真情况</h3>
          <div>
            {filt.verify.map((ite, i) => {
              return (
                <span
                  className={verify === i ? style.active : null}
                  key={i}
                  onClick={() => changeverify(ite, i)}
                >
                  {ite}
                </span>
              );
            })}
          </div>
        </div>

        <div className={style.ori}>
          <h3>朝向</h3>
          <div>
            {filt.ori.map((ite, i) => {
              return (
                <span
                  className={ori === i ? style.active : null}
                  onClick={() => changeori(ite, i)}
                  key={i}
                >
                  {ite}
                </span>
              );
            })}
          </div>
        </div>
      </div>

      <ul
        className={style.distance}
        style={{
          height: "200px",
          display: need === "距离" && props.layer ? "flex" : "none",
        }}
      >
        {dastance.map((ite, i) => {
          return (
            <li key={i} onClick={() => changedastance(ite)}>
              {ite}
            </li>
          );
        })}
      </ul>

      <ul
        ref={sort}
        className={style.sort}
        style={{
          height: "200px",
          bottom:
            props.history.location.pathname === "/mapsearch"
              ? `-200px`
              : -bottom - 44 + "px",
          display: need === "排序" && props.layer ? "flex" : "none",
        }}
      >
        {sortlist.map((ite, i) => {
          return (
            <li key={i} onClick={() => changesort(ite)}>
              {ite}
            </li>
          );
        })}
      </ul>

      <button
        style={{
          display: btn ? "block" : "none",
          background: "red",
          width: "100%",
          height: "44px",
          color: "#fff",
          textAlign: "center",
          lineHeight: "44px",
          position: "fixed",
          bottom: 0,
          left: 0,
          border: "none",
          outline: "none",
          borderRadius: "5px",
        }}
        onClick={submit}
      >
        确认
      </button>
    </div>
  );
};

export default withRouter(MapSelection);
