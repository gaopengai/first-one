import React, { Component } from "react";
import BScroll from "better-scroll";
import style from "./index.module.scss";

class ScrollComponent extends Component {
  componentDidMount() {
    new BScroll(this.swiperScroll, {
      observeDOM: true,
      scrollY: true,
      click: true,
    });
  }
  render() {
    return (
      <div className={style.container} ref={(ref) => (this.swiperScroll = ref)}>
        {this.props.children}
      </div>
    );
  }
}

export default ScrollComponent;
