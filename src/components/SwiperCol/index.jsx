import SwiperCore, { Pagination, Autoplay } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/swiper.scss";
import "swiper/components/pagination/pagination.scss";
import style from "./index.module.scss";

SwiperCore.use([Pagination, Autoplay]);

const SwiperCol = ({ swiperdata }) => {
  return (
    <Swiper
      className={style.swp}
      autoplay
      loop={true}
      direction="vertical"
      slidesPerView={2}
      onSwiper={() => {}}
      onSlideChange={() => {}}
    >
      {swiperdata.map((item) => {
        return (
          <SwiperSlide key={item.id} className={style.ppp}>
            <span>动态</span>
            <p className={style.item}>{item.title}</p>
          </SwiperSlide>
        );
      })}
    </Swiper>
  );
};

export default SwiperCol;
