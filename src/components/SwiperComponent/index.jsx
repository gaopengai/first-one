import SwiperCore, { Pagination, Autoplay } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/swiper.scss";
import "swiper/components/pagination/pagination.scss";
import style from "./index.module.scss";

SwiperCore.use([Pagination, Autoplay]);

const SwiperComponent = ({ swiperdata }) => {
  return (
    <Swiper
      autoplay
      loop={true}
      pagination={{ clickable: true }}
      onSwiper={() => {}}
      onSlideChange={() => {}}
    >
      {swiperdata.map((item) => {
        return (
          <SwiperSlide key={item.id}>
            <img
              className={style.pic}
              src={"http://47.102.145.189:8009" + item.imgSrc}
              alt=""
            />
          </SwiperSlide>
        );
      })}
    </Swiper>
  );
};

export default SwiperComponent;
