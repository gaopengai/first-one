import React from "react";
import ReactDOM from "react-dom";
import Router from "@/router/Router";
import routerlist from "@/router/routerList";
import FancyComponent from "@/components/FancyComponent";
import "./utils/setRem";
import Vconsole from "vconsole";
import "./index.scss";
import "zarm/dist/zarm.css";
import "@/font/iconfont.css";
import "@/icon/iconfont.css";
import { Provider } from "react-redux";
import store from "@/store";

// 平时web开发时，在手机上，如果是要看控制台信息，都需要alert弹窗，这样很不友好.还会阻拦进程。
// 通过vConsole.js 重写console方法，实现了类似于微信小程序的移动端调试效果。
if (process.env.NODE_ENV === "development") {
  new Vconsole();
}

ReactDOM.render(
  <FancyComponent>
    <Provider store={store}>
      <Router config={routerlist}></Router>
    </Provider>
  </FancyComponent>,
  document.getElementById("root")
);


