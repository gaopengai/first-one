import ErPages from "@/components/ErPage";
import style from "./index.module.scss";

const Anyang = (props) => {
  const goback = () => {
    props.history.go(-1);
  };
  return (
    <div className={style.erall}>
      <div className={style.erall_header}>
        <p onClick={() => goback()}>&lt;</p>
        <p>二手房</p>
        <i></i>
      </div>
      <div className={style.erall_con}><ErPages></ErPages></div>
    </div>
  );
};

export default Anyang;
