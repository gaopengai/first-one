import style from "./index.module.scss";
import { useState, useEffect } from "react";
import classNames from "classnames";
import { useHistory } from "react-router-dom";
import { agentMemberList, agentStoreList, companyList } from "@/api/api";
const Broker = (props) => {
  let route = useHistory();
  const [toplists, settoplists] = useState([]);
  const [nums] = useState([]);
  const [lists, setlists] = useState([]);
  const [ind, setind] = useState(0);
  const [page] = useState(1);
  const [rows] = useState(10);
  const [type, settype] = useState(1);
  const [keywords] = useState("");
  const [store_id] = useState(0);
  const [color, setColor] = useState("rgb(241, 100, 100)");
  const [list] = useState(["活跃榜", "优选门店", "实力榜"]);
  const _agentMemberLists = async () => {
    const res = await agentMemberList({ page, rows, type, keywords, store_id });
    for (let i = 1; i < res.data.list.length + 1; i++) {
      let a = i < 10 ? `0${i}` : i;
      nums.push(a);
      res.data.list.forEach((item, index) => {
        item.num = nums[index];
      });
      setlists(res.data.list.slice(3, res.data.list.length));
      settoplists(res.data.list.slice(0, 3));
    }
    const goback = () => {
      route.go(-1);
    };
    const _agentMemberList = async (index, event) => {
      if (index === 0) {
        setind(index);
        setColor("rgb(241, 100, 100)");
        _agentMemberLists();
      } else if (index === 1) {
        settype(2);
        setColor("rgb(0, 202, 167)");
        setind(index);
        let res = await agentStoreList({
          page,
          rows,
          type,
          keywords,
          store_id,
        });
        for (let i = 1; i < res.data.list.length + 1; i++) {
          let a = i < 10 ? `0${i}` : i;
          nums.push(a);
          res.data.list.forEach((item, index) => {
            item.num = nums[index];
          });
          setlists(res.data.list.slice(3, res.data.list.length));
          settoplists(res.data.list.slice(0, 3));
        }
      } else if (index === 2) {
        settype(3);
        setColor("rgb(76, 199, 246)");
        setind(index);

        let res = await companyList({ page, rows, type, keywords, store_id });
        for (let i = 1; i < res.data.list.length + 1; i++) {
          let a = i < 10 ? `0${i}` : i;
          nums.push(a);
          res.data.list.forEach((item, index) => {
            item.num = nums[index];
          });
          setlists(res.data.list.slice(3, res.data.list.length));
          settoplists(res.data.list.slice(0, 3));
        }
      }
    };
    useEffect(() => {
      _agentMemberLists();
    }, []);
    const goDetail = (id) => {
      route.push(`/detail/${id}`);
    };
    return (
      <div className={style.broker}>
        <div className={style.Collent_header}>
          <p
            onClick={() => {
              props.history.go(-1);
            }}
          >
            &lt;
          </p>
          <p>经纪人</p>
          <p></p>
        </div>
        <div className={style.search} style={{ backgroundColor: color }}>
          <input
            className={style.inp}
            type="text"
            placeholder="请输入需要查询的经纪人"
            name=""
            id=""
          />
          <div className={style.line}>
            {toplists.map((item, index) => {
              return (
                <span key={item.id}>
                  <img
                    className={style.imgs}
                    src={item.img || item.image || item.logo}
                    alt=""
                  />
                  <i
                    className="iconfont icon-yajun1"
                    item
                    style={{ color: "rgb(221, 131, 13)", fontSize: "40px" }}
                  ></i>
                  <b>{item.cname ? item.cname : item.name}</b>
                </span>
              );
            })}
          </div>
          <div className={style.list}>
            <div className={style.title}>
              {list.map((item, index) => {
                return (
                  <span
                    key={index}
                    onClick={(event) => {
                      _agentMemberList(index, event);
                    }}
                    className={classNames(
                      index !== ind
                        ? style.class
                        : index === 0
                        ? style.classfirst
                        : index === 1
                        ? style.classsecond
                        : index === 2
                        ? style.classthird
                        : ""
                    )}
                  >
                    {item}
                  </span>
                );
              })}
            </div>
            <ul className={style.brokerList}>
              {lists.map((item) => {
                return (
                  <li key={item.id} onClick={() => goDetail(item.id)}>
                    <span className={style.code}>{item.num}</span>
                    <div className={style.la}>
                      <img
                        className={style.userImg}
                        src={item.img || item.image || item.logo}
                        alt=""
                      />
                    </div>
                    <div className={style.con}>
                      <div className={style.top}>
                        {item.cname ? item.cname : item.name}
                      </div>

                      <div className={style.bot}>
                        {item.agent_num
                          ? `经纪人${item.agent_num}人`
                          : `二手房${item.house_count}套`}
                      </div>
                    </div>
                    <div className={style.ra}>
                      <i
                        className="iconfont icon-icon-test"
                        style={{
                          color: color,
                          fontSize: "20px",
                          paddingLeft: "10px",
                        }}
                      ></i>
                      <i
                        className="iconfont icon-icon-test1"
                        style={{
                          color: color,
                          fontSize: "20px",
                          paddingLeft: "10px",
                        }}
                      ></i>
                    </div>
                  </li>
                );
              })}
            </ul>
          </div>
        </div>
      </div>
    );
  };
};
export default Broker;
