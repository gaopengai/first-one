import { Icon } from "antd-mobile";
import { useState, useEffect } from "react";
import Header from "@/components/HeaderComponent";
import style from "./buyhouse.module.scss";
import Select from "@/components/Select";
import { getcity, hotfloor } from "@/api/api.js";
import MapSelection from "@/components/MapSelection";
import ScrollComponent from "@/components/ScrollComponent";

const BuyHouse = ({ history }) => {
  // 城市
  const [citylist, setcitylist] = useState([]);
  const [flag, setflag] = useState(false);
  const [layer, setlayer] = useState(false);
  const [hotfloordata, sethotfloor] = useState([]);
  const navlist = [
    {
      title: "区域",
      icon: "down",
    },
    {
      title: "价格",
      icon: "down",
    },
    {
      title: "户型",
      icon: "down",
    },
    {
      title: "筛选",
      icon: "down",
    },
  ];
  const typelist = [
    {
      img: "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fbpic.588ku.com%2Felement_origin_min_pic%2F19%2F04%2F22%2Ff1b5a3cf906b809e6c2a19b97b1fe933.jpg&refer=http%3A%2F%2Fbpic.588ku.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1632469505&t=82c497b047d2097349e98578a8cece40",
      title: "找商铺",
    },
    {
      img: "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fpic.51yuansu.com%2Fpic3%2Fcover%2F01%2F96%2F42%2F5983a4473cb85_610.jpg&refer=http%3A%2F%2Fpic.51yuansu.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1632469580&t=4a7d0ed57cff1d87d1f2b4f4fbf41fec",
      title: "找写字楼",
    },
    {
      img: "https://img2.baidu.com/it/u=692326734,339477779&fm=26&fmt=auto&gp=0.jpg",
      title: "找小区",
    },
  ];
  const [actived, setactiveed] = useState(0);
  const init = async () => {
    let res = await getcity();
    if (res.status === 200) {
      setcitylist(res.data.body);
    }
    const result = await hotfloor();
    if (result.status === 200) {
      sethotfloor(result.data.hotBuild.hotBuild);
      setflag(true);
    }
  };
  const changecity = (ite) => {
    console.log(ite);
  };
  const changelayer = (f) => {
    setlayer(f);
  };
  const goback = () => {
    history.go(-1);
  };
  const gofilrate = (e) => {
    if (
      e.target.classList.contains("区域") ||
      e.target.classList.contains("价格") ||
      e.target.classList.contains("户型") ||
      e.target.classList.contains("筛选")
    ) {
      history.push("/filrate");
    }
  };
  const tohotfloorDetail = (id) => {
    history.push(`/tohotfloordetail/${id}`);
  };
  useEffect(() => {
    init();
  }, []);
  return (
    <div style={{ width: "100%", height: "100%" }}>
      {flag ? (
        <div className={style.buyhouse}>
          <Header
            title="买房"
            goback={goback}
            back="block"
            height="90px"
          ></Header>
          <div className={style.bhnav1}>
            <div className={style.bhsearchtit}>
              <Select
                list={citylist}
                getCity={changecity}
                width="80px"
                height="30px"
              ></Select>
              <div className={style.bhsearch}>
                <input type="text" placeholder="请在这里输入区域/商圈" />
                <Icon type="search" size="xs" />
              </div>
              <div
                className={style.bhmapfh}
                onClick={() => history.push("/mapsearch")}
              >
                地图找房
              </div>
            </div>

            <div className={style.bhchoice}>
              {typelist.map((ite, i) => {
                return (
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      alignItems: "center",
                    }}
                    key={i}
                    onClick={() => setactiveed(i)}
                  >
                    <img src={ite.img} width="60px" height="60px" alt="" />
                    <span className={actived === i ? style.active : null}>
                      {ite.title}
                    </span>
                  </div>
                );
              })}
            </div>
          </div>
          <div onClick={(e) => gofilrate(e)} className={style.selectbox}>
            <MapSelection
              list={navlist}
              city={citylist}
              changelayer={changelayer}
              layer={layer}
            ></MapSelection>
          </div>
          <div className={style.bhouse}>
            <ScrollComponent>
              <div className={style.bhcon}>
                {hotfloordata.map((item) => {
                  return (
                    <dl key={item.id} onClick={() => tohotfloorDetail(item.id)}>
                      <dt>
                        <img src={item.img} alt="" />
                      </dt>
                      <dd>
                        <h3>
                          {item.title}
                          <span
                            className={style.Hotproperty_con_status_name}
                            style={{ background: item.status_color }}
                          >
                            {item.status_name}
                          </span>
                        </h3>
                        <p className={style.Hotproperty_con_price}>
                          {item.build_price}
                          <span>元/m²</span>
                        </p>
                        <p className={style.Hotproperty_con_type}>
                          {item.b_type}
                        </p>
                        <p className={style.Hotproperty_con_kfs}>{item.kfs}</p>
                      </dd>
                    </dl>
                  );
                })}
              </div>
            </ScrollComponent>
          </div>
          <div
            className={style.fix}
            onClick={() => {
              setlayer(false);
            }}
            style={{
              width: "100%",
              height: "100%",
              background: "rgba(0,0,0,.5)",
              position: "absolute",
              bottom: 0,
              left: 0,
              top: 0,
              right: 0,
              display: layer ? "block" : "none",
            }}
          ></div>
        </div>
      ) : (
        <div>loading</div>
      )}
    </div>
  );
};

export default BuyHouse;
