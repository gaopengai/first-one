import React from 'react'
import {useState} from "react"
import {contactDetails,chatLog} from "@/api/api"
import { useEffect } from 'react'
import Header from "@/components/HeaderComponent"
import {useHistory} from "react-router-dom"
import style from "./index.module.scss"
import classNames from 'classnames'
export default function Chatdetail(props) {
   let route=useHistory()
   const [user_id]=useState(props.match.params.id)
   const [chat_id]=useState(props.match.params.chat_id)
   const [page]=useState(1)
   const [user,setUser]=useState("")
   const [list,setChatList]=useState([])
   const [content,setcontent]=useState({})
   const _contactDetails=()=>{
    contactDetails({user_id}).then(res=>{
        setUser(res.data.member.nickname)
    })
   }
   const _chatLog=()=>{
    chatLog({chat_id,page}).then(res=>{
        console.log(res);
        setChatList(res.data.list)
        // res.data.list.map(item=>)
       let obj=JSON.parse(res.data.list[1].content)
       console.log("----",JSON.parse(res.data.list[1].content));
       console.log(Object.prototype.toString.call(JSON.parse(res.data.list[1].content))==="[object Object]");
      
    //    for(let key in obj){
    //        console.log(key);
    //    }
       setcontent(obj)
    //    {Object.prototype.toString.call(item.content)
    //        a.innerHTML=
    //   const data=(res.data.list[1].content).split(" ")
    //   console.log(data);
    //   console.log(data[0].substr(1,data[0].lengths));
    
  

      console.log(Object.prototype.toString.call(content));
      console.log(Object.prototype.toString.call(content)==="[object Object]");
  
     

    })
   }
   const goback=()=>{
    route.go(-1)
   }
  
   useEffect(()=>{
    _contactDetails()
    _chatLog()
   },[])

    return (
       
        <div className={style.wraper}>
            <Header title={user}  back="block"
        goback={
          goback
        }></Header>
        <div className={style.list}>
            {
                list.map((item,index)=>{
                    return  <div key={item.id}>
                        <span className={classNames(
                style.tap,
                item.to_id==user_id?style.class : ""
              )}>{Object.prototype.toString.call(item.content)==="[object String]"?item.content:content.title}</span>
                </div>   
                })
            }
        </div>
        </div>
    )
}
