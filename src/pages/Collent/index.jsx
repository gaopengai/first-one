import { connect } from "react-redux";
import style from "./index.module.scss";
function Collent(props) {
  return (
    <div>
      <div className={style.Collent_header}>
        <p onClick={()=>{props.history.go(-1)}}> &lt;</p>
        <p>收藏</p>
        <p></p>
      </div>
      <div className={style.Hotproperty_con}>
        {props.list.length > 0 ? (
          props.list.map((item) => {
            return (
              <dl key={item.id}>
                <dt>
                  <img src={item.img} alt="" />
                </dt>
                <dd>
                  <h3>
                    {item.title}
                    <span
                      className={style.Hotproperty_con_status_name}
                      style={{ background: item.status_color }}
                    >
                      {item.status_name}
                    </span>
                  </h3>
                  <p className={style.Hotproperty_con_price}>
                    {item.build_price}
                    <span>元/m²</span>
                  </p>
                  <p className={style.Hotproperty_con_type}>{item.b_type}</p>
                  <p className={style.Hotproperty_con_kfs}>{item.kfs}</p>
                </dd>
              </dl>
            );
          })
        ) : (
          <h3>暂无收藏数据</h3>
        )}
      </div>
    </div>
  );
}
const mapStateToProps = (state) => {
  return {
    list: state.collentdata,
  };
};
export default connect(mapStateToProps)(Collent);
