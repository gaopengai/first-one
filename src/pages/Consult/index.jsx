import React from 'react'
import {useEffect,useRef,useState} from "react"
import { io } from  "socket.io-client" ;
import { Toast,Icon} from 'antd-mobile';
import style from "./index.module.scss"
import classNames from 'classnames';
import {useHistory} from "react-router-dom"
import { loadPartialConfig } from '@babel/core';
export default function Consult() {
const history=useHistory()
const socketItem=useRef(null)
const [list,setList]=useState([])
const [question,setValue]=useState("")
const [userinfo,setUserinfo]=useState({})
const [img,setimg]=useState("https://img2.baidu.com/it/u=3872272422,2400331248&fm=26&fmt=auto&gp=0.jpg")
    useEffect(()=>{
        
//链接服务器
socketItem.current=io('http://49.232.77.197:8081/robot')
//验证身份
socketItem.current.emit('authenticate',{token:window.sessionStorage.getItem("authorization")})
//链接事件
socketItem.current.on("connected",data=>{
    if(data.code===401){
        Toast.fail(data.msg,1,
           ()=>{
                history.replace("/login")
        });
        return;   
    }
    console.log(data.questionList);
    setUserinfo(data.userinfo)
    console.log(userinfo);
    setList(val=>([...val,{
        id:1,
        value:data.questionList
    }]))
})
socketItem.current.on('message',data=>{
    console.log(data);
    setList(val=>([...val,{
        id:1,
        value:data.data
    }]))



})
    },[])
const handleClick=(question,id)=>{
    socketItem.current.emit('message',{id})
    setList(val=>[...val,{
        id:2,
        value:question 
    }])
    console.log(list);
}
//输入框输入时
const changeValue=(e)=>{
   
    if(e.keyCode===13){
        socketItem.current.emit('message',{type:"talk",value:e.target.value})
        setList(valitem=>[
            ...valitem,
           { id:2,
            value:e.target.value
        }
        ]
          )
    }
 
  
}
    return (
        <div className={style.chatList}>
            <div className={style.list}>
            {
                list.map(item=>{
                    return item.id===1?( <div key={item.id} className={classNames(style.nav,style.nav_left)}>
                    <img className={style.imgs} src={img} alt="" />
                    <span>{Array.isArray(item.value)?item.value.map(it=>{
                        return <b key={it.id} onClick={()=>handleClick(it.question,it.id)}>{it.question}</b>
                    }):item.value}</span>

                </div>):( 
                <div key={item.id} className={classNames(style.nav,style.nav_right)}>
                        <span>{item.value}</span>
                        <img src={userinfo?.avatar} alt="" />
                    </div>
                )
                })
            }  
            </div>
            <div className={style.inp}>
            <i
                  className="iconfont icon-yuyin"
                  style={{fontSize:"30px"}}
                ></i>
                <input type="text" onKeyDown={(e)=>{
                    changeValue(e)
                }} />
              
                <i
                  className="iconfont icon-biaoqing"
                  style={{fontSize:"35px"}}
                ></i>
                <i
                  className="iconfont icon-add-circle"
                  style={{fontSize:"38px"}}
                ></i></div>
        </div>
    )
}
