import Header from "@/components/HeaderComponent";
import style from "./index.module.scss";
import { useHistory } from "react-router-dom";
import { useState, useEffect } from "react";
import { agentInfo, agentDetail } from "@/api/api";

const Detail = (props) => {

  const [id] = useState(props.match.params.id);
  const [sid] = useState("");
  const [sharetype] = useState("");
  const [cate_id] = useState(1);
  const [keyword] = useState("");
  const [page] = useState(1);
  const [house, sethouse] = useState([]);
  const [userInfo,setInfo]=useState({
    cname:"",
    img:"",
    tname:"",
    tel:"",
    address:""
  })

  const route = useHistory();
  const goback = () => {
    route.go(-1);
  };
  const _agentInfo = async () => {
    console.log(props.match.params.id);
     const res=await agentInfo({id,sid,sharetype})
         setInfo({...userInfo,cname:res.data.agent.cname,img:res.data.agent.img,tname:res.data.agent.tname,tel:res.data.agent.tel, address:res.data.agent.address})
 }
 const _agentDetail=async()=>{
     const res=await agentDetail({page,cate_id,keyword,sid,sharetype,id})
     console.log(res); 
     sethouse(res.data.house)    
 }
 const goconsult=()=>{
  route.push("/consult")
   
 }

  useEffect(() => {
    _agentInfo();
    _agentDetail();
  }, []);

  return (
    <div className={style.my}>
      <div className={style.heard}>
        <Header
          title="详情"
          back="block"
          goback={() => {
            goback();
          }}
        ></Header>
      </div>
      <div className={style.float}>
        <div className={style.up}>
          <div className={style.la}>
            <p>{userInfo.tname}</p>
            <h3>{userInfo.cname}</h3>
            <p>{userInfo.address}</p>
          </div>
          <div className="ra">
            <img className={style.imgs} src={userInfo.img} alt="" />
          </div>
        </div>
        {/* <div className={style.down}></div> */}
      </div>
      <div className={style.bottom}>
        <div className={style.content}>
          <div className={style.title}>
            <span className={style.sale}>卖房</span> <span>租房</span>
          </div>

          {house.map((item) => {
            return (
              <div className={style.Items} key={item.id}>
                <div className={style.left}>
                  <img className={style.image} src={item.img} alt="" />
                </div>
                <div className={style.right}>
                  <span>{item.title}</span>
                  <p>
                    {" "}
                    <span>
                      {item.shi}室{item.ting}厅
                    </span>
                    |<span>{item.cmname}</span>
                    <span>{item.mianji}m</span>
                  </p>
                  <b>{item.fangjia}万</b>
                </div>
              </div>
            );
          })}
        </div>
      </div>
      <div className={style.footer}>
        <button className={style.btn1} onClick={goconsult}>在线咨询</button><button className={style.btn2}>电话咨询</button>

      </div>
    </div>
  );
};

export default Detail;
