import React, { Component } from "react";
import ReactEcharts from "echarts-for-react";
import * as echarts from "echarts";
import style from "./index.module.scss";

class Echarts extends Component {
  state = {
    xdata: [],
    ydata: [],
  };

  componentDidMount() {
    const data = this.props.history.location.state;
    console.log(data);
    let xxx = [3, 4, 5];
    let yyy = [data.minHuxing, data.maxHuxing, data.price];
    this.setState({
      xdata: xxx,
      ydata: yyy,
    });
  }

  //  返回
  goback() {
    this.props.history.go(-1);
  }

  render() {
    let { xdata, ydata } = this.state;
    // 指定图表的配置项和数据
    let option = {
      backgroundColor: new echarts.graphic.LinearGradient(
        0,
        0,
        0,
        1,
        [
          {
            offset: 0,
            color: "#c86589",
          },
          {
            offset: 1,
            color: "#06a7ff",
          },
        ],
        false
      ),
      // 鼠标滑过显示的数据
      tooltip: {
        show: true,
      },
      title: {
        text: "价格走势",
        left: "center",
        bottom: "5%",
        textStyle: {
          color: "#fff",
          fontSize: 16,
        },
      },
      grid: {
        top: "20%",
        left: "10%",
        right: "10%",
        bottom: "15%",
        containLabel: true,
      },
      xAxis: {
        type: "category",
        boundaryGap: false,
        data: ["1", "2", ...xdata],
        axisLabel: {
          margin: 30,
          color: "#ffffff63",
        },
        axisLine: {
          show: false,
        },
        axisTick: {
          show: true,
          length: 25,
          lineStyle: {
            color: "#ffffff1f",
          },
        },
        splitLine: {
          show: true,
          lineStyle: {
            color: "#ffffff1f",
          },
        },
      },
      yAxis: [
        {
          type: "value",
          position: "right",
          axisLabel: {
            margin: 20,
            color: "#ffffff63",
          },

          axisTick: {
            show: true,
            length: 15,
            lineStyle: {
              color: "#ffffff1f",
            },
          },
          splitLine: {
            show: true,
            lineStyle: {
              color: "#ffffff1f",
            },
          },
          axisLine: {
            lineStyle: {
              color: "#fff",
              width: 2,
            },
          },
        },
      ],
      series: [
        {
          name: "价格",
          type: "line",
          smooth: true, //是否平滑曲线显示
          showAllSymbol: true,
          symbol: "circle",
          symbolSize: 6,
          lineStyle: {
            normal: {
              color: "#fff", // 线条颜色
            },
          },
          label: {
            show: true,
            position: "top",
            textStyle: {
              color: "#fff",
            },
          },
          itemStyle: {
            color: "red",
            borderColor: "#fff",
            borderWidth: 3,
          },
          data: [1500, 1888, ...ydata],
        },
      ],
    };
    return (
      <div className={style.all}>
        <div className={style.header}>
          <p onClick={() => this.goback()}>&lt;</p>
          <p> 逸品尚居房价走势 </p>
          <i></i>
        </div>
        <ReactEcharts
          style={{ width: "100%", height: 300, left: 0 }}
          // ref={(ref) => (this.myecharts = ref)}
          option={option}
        />
      </div>
    );
  }
}

export default Echarts;
