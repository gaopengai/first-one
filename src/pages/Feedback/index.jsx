import { useState, useEffect } from "react";
import HeaderComponent from "@/components/HeaderComponent";
import style from "./index.module.scss";
import { useHistory } from "react-router-dom";
import { List, TextareaItem, Button } from "antd-mobile";
import { createForm } from "rc-form";
function Feedback(props) {
  let history = useHistory();
  // const [list, setList] = useState({});

  useEffect(() => {}, []);
  const goback = () => {
    history.go(-1);
  };
  const { getFieldProps } = props.form;
  return (
    <div className={style.question}>
      <HeaderComponent title="问题反馈" back="block" goback={() => goback()} />
      <div className={style.phone}>
        <p>联系方式</p>
        <input type="text" placeholder="请输入手机号" />
      </div>
      <div className={style.content}>
        <List renderHeader={"问题反馈"}>
          <TextareaItem
            {...getFieldProps("count", {
              initialValue: "",
            })}
            rows={5}
            count={200}
            placeholder="请输入"
          />
        </List>
      </div>
      <Button type="warning">提交反馈</Button>
    </div>
  );
}
export default createForm()(Feedback);
