import style from "./filter.module.scss";
import Header from "@/components/HeaderComponent";
import MapSelection from "@/components/MapSelection";
import { getcity, getarea } from "@/api/api.js";
import { useEffect, useState } from "react";
const Filrate = (props) => {
  const navlist = [
    {
      title: "区域",
      icon: "down",
    },
    {
      title: "价格",
      icon: "down",
    },
    {
      title: "户型",
      icon: "down",
    },
    {
      title: "筛选",
      icon: "down",
    },
  ];
  const goback = () => {
    props.history.go(-1);
  };
  const [title, settitle] = useState("区域筛选");
  // 城市
  const [citylist, setcitylist] = useState([]);
  // 地区
  const [arealist, setlist] = useState([]);
  const init = async () => {
    let res = await getcity();
    if (res.status === 200) {
      setcitylist(res.data.body);
    }
  };
  const gettit = (tit) => {
    settitle(tit + "筛选");
  };
  const getid = (id) => {
    getarea({ id }).then((res) => {
      res.status === 200 && setlist(res.data.body);
    });
  };
  useEffect(() => {
    init();
  }, []);
  return (
    <div className={style.filrate}>
      <Header title={title} height="60px" back="block" goback={goback}></Header>
      <MapSelection
        list={navlist}
        city={citylist}
        onid={getid}
        area={arealist}
        ontitle={gettit}
      ></MapSelection>
    </div>
  );
};

export default Filrate;
