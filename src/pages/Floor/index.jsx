
import style from "./index.module.scss"
import Header from "@/components/HeaderComponent"
import {building_circle} from "@/api/api"
import {useState,useEffect} from "react"
import { Button} from 'antd-mobile';
import {useHistory} from "react-router-dom"

const Floor=()=>{
    const route=useHistory()
    const [page]=useState(1)
    const [cate]=useState(1)
    const [type]=useState(0)
    const [cate_id]=useState(0)
    const [sort]=useState(1)
    const [keywords]=useState("")
    const [list,setList]=useState([])

    const goback=()=>{
        route.push("/index/home")
    }
    const _building_circle=()=>{
        building_circle({page,cate,type,cate_id,sort,keywords}).then(res=>{
            console.log(res);
            setList(res.data.lists)
        })
    }
    useEffect(()=>{
        _building_circle()
    },[])
    return (
        <div className={style.floorWraper}>
            <Header title="楼市圈" back="block" goback={()=>{goback()}}></Header>
            <ul className={style.list}>
                {list.map((item,index)=>{
                    return (
                     <li key={index}>
                     <div className={style.top}>
                         <div className={style.la}><img className={style.imgs} src={item.prelogo} alt="" /></div>
                         <div className={style.con}>   
                             <div className={style.con_top}>
                                 {item.cname}
                             </div>
                             <div className={style.con_bot}>
                                {item.adviser_level_name}
                             </div>
                           </div>
                           <div className={style.ra}>
                               <p>
                               <Button className={style.but} type="ghost" inline size="small" style={{ marginRight: '4px' }}>在线咨询</Button>
                          <Button className={style.but} type="ghost" inline size="small" style={{ marginRight: '4px' }}>拨打电话</Button>

                               </p>
                        
                           </div>
                     </div>
                     <div className={style.bot}>
                                     <div className={style.left}>
                                         <h2>8月</h2>
                                         <span>{item.ctime.slice(5,11)}</span>
                                     </div>
                                     <div className={style.right}>
                                         <div className={style.title}>
                                              {item.content}
                                         </div>
                                         <div className={style.imgMsg}>
                                          {
                                                item.attached.map((it,ind)=>{
                                                 return <img key={ind} className={style.imgPath} src={it.path} alt="" />
                                                 })
                                          }
                                         </div>
                                 </div>
                       </div>
                 </li>)

                })}
               
                
                
            </ul>


        </div>
    )
}
export  default Floor