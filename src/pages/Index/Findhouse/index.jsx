import Header from "@/components/HeaderComponent";
import { Icon, Button, Toast } from "antd-mobile";
import style from "./index.module.scss";
import classNames from "classnames";
import { getpublish } from "@/api/api.js";
import { useState } from "react";

const Findhouse = (props) => {
  const [form, setform] = useState({
    city: "",
    name: "",
    phone: "",
  });
  const [flag, setflag] = useState(true);
  const changecity = ({ target }) => {
    setform({
      ...form,
      city: target.value,
    });
  };
  const changename = ({ target }) => {
    setform({
      ...form,
      name: target.value,
    });
  };
  const changephone = ({ target }) => {
    setform({
      ...form,
      phone: target.value,
    });
  };
  const publish = async () => {
    let res = flag && (await getpublish());
    setflag(false);
    setTimeout(() => {
      setflag(true);
    }, 1000 * 60 * 5);
    if (res.status === 200) {
      Toast.success("发布成功 !!!", 1);
    } else {
      Toast.fail("您发布频率过快，请5分钟后再发布 !!!", 1);
    }
  };
  return (
    <div className={style.findhouse}>
      <Header title="找房" space="10px" valign={false}></Header>

      <div className={style.fhcontainer}>
        <div className={style.fhheader}>
          <div className={style.helpmefh}>
            <div className={style.helptit}>
              <h4>帮我找房</h4>
            </div>
            <div className={style.helpcon}>
              <div className={style.helpcity}>
                <Icon type="search" />
                <input
                  type="text"
                  placeholder="请输入意向城市、区域、意向楼盘"
                  value={form.city}
                  onChange={(e) => changecity(e)}
                />
              </div>
              <div className={style.helpinfo}>
                <div className={style.helpname}>
                  <input
                    type="text"
                    placeholder="请输入姓名"
                    value={form.name}
                    onChange={(e) => changename(e)}
                  />
                </div>
                <div className={style.helpphone}>
                  <input
                    type="text"
                    placeholder="请输入手机号"
                    value={form.phone}
                    onChange={(e) => changephone(e)}
                  />
                </div>
              </div>
              <p>
                <Icon type="check-circle" size="xxs" />
                <span>隐私保护已开启</span>
              </p>
              <div className={style.helpbtn}>
                <Button type="warning" onClick={publish}>
                  发布需求
                </Button>
              </div>
              <div className={style.helpnum}>当前有148位置业顾问为您服务</div>
            </div>
          </div>
        </div>
        <div className={style.wrapper}></div>
        <div className={style.options}>
          <div
            onClick={() => props.history.push("/buyhouse")}
            className={classNames(style.option, style.option1)}
          >
            <div className={style.helpoption}>
              <div className={style.opleft}>
                <span>新房</span>
                <p>热门新楼盘&gt;</p>
              </div>
              <i></i>
            </div>
          </div>
          <div
            onClick={() => props.history.push("/mapsearch")}
            className={classNames(style.option, style.option2)}
          >
            <div className={style.helpoption}>
              <div className={style.opleft}>
                <span>地图找房</span>
                <p>查看附近好房源&gt;</p>
              </div>
              <i></i>
            </div>
          </div>
          <div
            className={classNames(style.option, style.option3)}
            onClick={() => props.history.push("/broker")}
          >
            <div className={style.helpoption}>
              <div className={style.opleft}>
                <span>经纪人</span>
                <p>优选置业顾问&gt;</p>
              </div>
              <i></i>
            </div>
          </div>
          <div
            onClick={() => props.history.push("/floor")}
            className={classNames(style.option, style.option4)}
          >
            <div className={style.helpoption}>
              <div className={style.opleft}>
                <span>楼市圈</span>
                <p>楼市最新状态&gt;</p>
              </div>
              <i></i>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Findhouse;
