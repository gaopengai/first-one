import React, { useEffect, useState } from "react";
import HeaderComponent from "@/components/HeaderComponent";
import SwiperComponent from "@/components/SwiperComponent";
import SwiperCol from "@/components/SwiperCol";
import { swiper, hotfloor } from "@/api/api";
import style from "./index.module.scss";
import homeListdata from "@/utils/homeListData";
import ScrollComponent from "@/components/ScrollComponent";
import ErPage from "@/components/ErPage";
import { tohotaction } from "../../../store/action/action";
import { connect } from "react-redux";

const Home = (props) => {
  // 搜索  子传父
  const tosearchpage = () => {
    props.history.push("/searchpage");
  };
  // ======================================================================
  const [swiperdata, setswiperdata] = useState([]); // 轮播图
  // 请求轮播图数据
  const getSwiperdata = async () => {
    const res = await swiper();
    if (res.status === 200) {
      setswiperdata(res.data.body);
    }
  };
  useEffect(() => {
    getSwiperdata();
  }, []);
  // ======================================================================
  const [itemdata, setitemdata] = useState([]); // nav 导航 列表item
  useEffect(() => {
    setitemdata(homeListdata);
  }, []);
  const navTo = (path) => {
    props.history.push(path);
  };
  // ======================================================================
  const [advisorydata, setadvisory] = useState([]); // 楼市资讯
  const [hotfloordata, sethotfloor] = useState([]); // 热门楼盘
  const gethotfloordata = async (props) => {
    // 请求热门楼盘数据
    const res = await hotfloor();
    if (res.status === 200) {
      setadvisory(res.data.news.news);
      sethotfloor(res.data.hotBuild.hotBuild);
    }
  };
  useEffect(() => {
    gethotfloordata();
  }, []);
  // ======================================================================
  // 点击跳详情
  const tohotfloorDetail = (id, item) => {
    props.history.push(`/tohotfloordetail/${id}`);
    
  };
  // ======================================================================
  return (
    <div className={style.home}>
      <HeaderComponent
        title="宏业找房"
        align="left"
        search="block"
        // height="90px"
        onfocus={() => tosearchpage()}
      ></HeaderComponent>
      <ScrollComponent>
        <div className={style.home_con}>
          <SwiperComponent swiperdata={swiperdata}></SwiperComponent>
          {/* 导航列表 */}
          <div className={style.home_list}>
            <ul className={style.home_list_con}>
              {itemdata.map((item) => {
                return (
                  <li key={item.name} onClick={() => navTo(item.path)}>
                    <img src={item.url} alt="" />
                    <b>{item.name}</b>
                  </li>
                );
              })}
            </ul>
          </div>
          {/* 楼市咨询 */}
          <div className={style.advisory}>
            <div className={style.advisory_top}>
              <h2>楼市资讯</h2>
              {/* <p>更多</p> */}
            </div>
            <div className={style.advisory_con}>
              <div className={style.advisory_con_tit}>
                <div className={style.advisory_con_tit_zuo}>
                  <h3>
                    10930.56<span>元/㎡</span>
                  </h3>
                  <p>二手房挂牌实时均价</p>
                </div>
                <div className={style.advisory_con_tit_you}>
                  <h3>
                    12560.00<span>元/㎡</span>
                  </h3>
                  <p>新房均价</p>
                </div>
              </div>
              <div className={style.advisory_con_box}>
                <SwiperCol swiperdata={advisorydata}></SwiperCol>
              </div>
            </div>
          </div>
          {/* 热门楼盘 */}
          <div className={style.Hotproperty}>
            <div className={style.Hotproperty_top}>
              <h2>热门楼盘</h2>
              <p>更多</p>
            </div>
            <div className={style.Hotproperty_con}>
              {hotfloordata.map((item) => {
                return (
                  <dl
                    key={item.id}
                    onClick={() => tohotfloorDetail(item.id, item)}
                  >
                    <dt>
                      <img src={item.img} alt="" />
                    </dt>
                    <dd>
                      <h3
                        onClick={(e) => {
                          e.preventDefault();
                          props.tobrow(item);
                        }}
                      >
                        {item.title}
                        <span
                          className={style.Hotproperty_con_status_name}
                          style={{ background: item.status_color }}
                        >
                          {item.status_name}
                        </span>
                      </h3>
                      <p className={style.Hotproperty_con_price}>
                        {item.build_price}
                        <span>元/m²</span>
                      </p>
                      <p className={style.Hotproperty_con_type}>
                        {item.b_type}
                      </p>
                      <p className={style.Hotproperty_con_kfs}>{item.kfs}</p>
                    </dd>
                  </dl>
                );
              })}
            </div>
          </div>
          {/* 二手房 */}
          <ErPage></ErPage>
        </div>
      </ScrollComponent>
    </div>
  );
};

const mapStateToprops = (state) => {
  return {
    list: state.historydata,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    tobrow(item) {
      dispatch(tohotaction(item));
    },
  };
};
export default connect(mapStateToprops, mapDispatchToProps)(Home);
