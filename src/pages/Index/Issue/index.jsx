import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import style from "./index.module.scss";
import HeaderComponent from "@/components/HeaderComponent";

const Issue = () => {
  const router = useHistory();
  const [issueList, setIssue] = useState([
    {
      icon: "https://images.tengfangyun.com/images/icon/ershoufang.png?x-oss-process=style/w_220",
      text: "二手房出租",
      desc: "发布二手车出售信息",
      path: "/tworental",
    },
    {
      icon: "https://images.tengfangyun.com/images/icon/zufang.png?x-oss-process=style/w_220",
      text: "有房出租",
      desc: "发布房源出租信息",
      path: "/rental",
    },
    {
      icon: "https://images.tengfangyun.com/images/icon/qiugou.png?x-oss-process=style/w_220",
      text: "我想买个房子",
      desc: "发布二手车出售信息",
      path: "/wantbuy",
    },
    {
      icon: "https://images.tengfangyun.com/images/icon/qiuzu.png?x-oss-process=style/w_220",
      text: "我想租个房子",
      desc: "发布求租意向信息",
      path: "/wantrental",
    },
    {
      icon: "https://images.tengfangyun.com/images/icon/xinfang.png?x-oss-process=style/w_220",
      text: "买新房 职业顾问帮你忙",
      desc: "提交购房意向买房顾问全程带看",
      path: "/lookhouse",
    },
  ]);
  const toPage = (path, ind) => {
    if (ind === 0) {
      router.push(`${path}?catid=${1}`);
    } else if (ind === 1) {
      router.push(`${path}?catid=${2}`);
    } else if (ind === 2) {
      router.push(`${path}?catid=${4}`);
    } else if (ind === 3) {
      router.push(`${path}?catid=${3}`);
    } else {
      router.push("/broker");
    }
  };
  useEffect(() => {});
  return (
    <div className={style.container}>
      <HeaderComponent title="发布"></HeaderComponent>
      <div className={style.con}>
        <div className={style.box}>
          {issueList &&
            issueList.map((item, index) => {
              return (
                <div
                  key={index}
                  className={style.itemIssue}
                  onClick={() => {
                    toPage(item.path, index);
                  }}
                >
                  <img src={item.icon} alt="" />
                  <div className={style.info}>
                    <p>{item.text}</p>
                    <p>{item.desc}</p>
                  </div>
                  <div className={style.color}> &gt; </div>
                </div>
              );
            })}
        </div>
      </div>
    </div>
  );
};

export default Issue;
