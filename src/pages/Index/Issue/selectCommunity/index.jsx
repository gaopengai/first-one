import { Link } from "react-router-dom";
import { SearchBar } from "zarm";
import { useState } from "react";
import leftBtn from "../../../../static/right1.png";
import style from "./index.module.scss";
import { getFuzzySearchList } from "../../../../api/api";
const SelectCommunity = () => {
	const [value, setValue] = useState("");
	const [searchList, setSearchList] = useState([]);

	const getSearchList = async (val) => {
		let res = await getFuzzySearchList({keyword:val});
		console.log(res,"搜索结果");
		if(res.data.code === 1 && searchList.length === 0 ) {
			setSearchList(res.data.list);
		}
		console.log(searchList);
	}	

	return (
		<div className={style.fuzzySearch}>
			<div className={style.house}>
				<Link to="../../index/issue" className={style.fan}>
					＜
				</Link>
				<b>选择小区</b>
				<p></p>
			</div>
			<div>
				<SearchBar
					shape="rect"
					value={value}
					onChange={setValue}
                    onBlur={()=> value && getSearchList(value)}
					onClear={() => setValue("")}
				/>
			</div>
			<div className={style.searchArr}> 
				{value? searchList && searchList.map((item,index)=>{
						return <div key={item.id} className={style.itemlist} onClick={()=>console.log(item.address)}>
							<span>{item.address}</span>
							<div className={style.img}>
								<img src={leftBtn} alt="" />
							</div> 
						</div>
					}) : <></>  }
			</div>
		</div>
	);
};

export default SelectCommunity;
