import React from 'react';
import { Link,useHistory } from 'react-router-dom';
import style from './index.module.scss'
import lkc6 from "@/static/lkc6.png"

const SuccessPay = () => {
    const router = useHistory();
    return <div className="issue1">
        <div className={style.house}>
            <Link onClick={()=>router.go(-1)} className={style.fan}>＜</Link>
            <b>支付</b>
            <p></p>
        </div>
        <div className={style.phone}>
            <img src={lkc6} alt="" className={style.over} />
        </div>
        <div className={style.order}>
            <p>订单编号</p>
            <p>18111123435883</p>
        </div>
        <div className={style.order}>
            <p>订单金额</p>
            <p>10.00元</p>
        </div>
        <div className={style.order}>
            <p>支付方式</p>
            <p>微信</p>
        </div>
        <div className={style.order}>
            <p>支付时间</p>
            <p>{new Date().toLocaleString()}</p>
        </div>
        <div className={style.publish}>
            <button className={style.look}>查看发布</button>
            <Link to="/index/issue" className={style.goon}>继续发布</Link>
        </div>
    </div>
}

export default SuccessPay;