import React, { useState, useEffect } from "react";
import { Link, useHistory } from "react-router-dom";
import { getAllIssueData } from "../../../../api/api";
import {
  Select,
  Cell,
  Input,
  Checkbox,
  FilePicker,
  NoticeBar,
  Icon,
} from "zarm";
import style from "./index.module.scss";
const TwoRental = (props) => {
  const router = useHistory();
  let params = props.location.search.split("=")[1];
  const [value1, setValue] = useState([]);
  const [wheelDefaultValue, setWheelDefaultValue] = useState([]);
  const [title, setTitle] = useState("");
  const [title1, setTitle1] = useState("");
  const [title2, setTitle2] = useState("");
  const [title3, setTitle3] = useState("");
  const [title4, setTitle4] = useState("");
  const [title5, setTitle5] = useState("");
  const [houseType, setHouseType] = useState([]);
  const [Household, setHousehold] = useState([]);
  const [Area, setArea] = useState([]);

  useEffect(() => {
    getallList();
  }, [Household, houseType]);
  const getallList = async () => {
    let { data } = await getAllIssueData({ parentid: params });
    if (data.code === 1 && Household.length === 0) {
      setHousehold(data.labels);
      setHouseType(data.catelist);
      setArea(data.area, "==");
    }
  };
  let newType = [];
  let newArea = [];
  houseType.forEach((item, index) => {
    newType.push({
      id: item.value,
      label: item.name,
      value: item.name,
    });
  });
  Area.forEach((item, index) => {
    newArea.push({
      id: item.areaid,
      label: item.areaname,
      value: item.areaname,
    });
  });
  return (
    <div className="homesale">
      <div className={style.house}>
        <Link to="/index/issue" className={style.fan}>
          ＜
        </Link>
        <b>二手房出售</b>
        <p></p>
      </div>
      <NoticeBar theme="danger" icon={<Icon type="warning-round" />}>
        发布本条信息将收费10.00元
      </NoticeBar>
      <Link className={style.upload} to="/uploadimg">
        <dl>
          <dt>
            {" "}
            <div className="file-picker-wrapper">
              <FilePicker className="file-picker-btn" disabled>
                <Icon type="add" size="lg" theme="danger" />
              </FilePicker>
            </div>
          </dt>
          <dd>
            <b style={{ fontWeight: `500` }}>上传图片</b>
            <p>
              只能上传房屋图片,不能含有文字,数字,网址,名片,水印等,所有类别图片总计20张
            </p>
          </dd>
        </dl>
        <div className={style.xiang}>＞</div>
      </Link>
      <Cell
        title="小区"
        className={style.city}
        onClick={() => router.push("/selectcommunity")}
      >
        <Input
          clearable
          type="text"
          placeholder="请选择小区或留空"
          value={title2}
          onChange={(value) => {
            setTitle2(value);
          }}
          onBlur={(value) => console.log(`onBlur: ${value}`)}
        />
      </Cell>
      <Cell title="房产类型" className={style.city}>
        <Select
          className={style.city1}
          value={value1}
          wheelDefaultValue={wheelDefaultValue}
          dataSource={newType}
          onOk={(selected) => {
            setValue(selected.map((item) => item.value1));
          }}
        />
      </Cell>
      <Cell title="区域" className={style.city}>
        <Select
          className={style.city1}
          value={value1}
          wheelDefaultValue={wheelDefaultValue}
          dataSource={newArea}
          onOk={(selected) => {
            setValue(selected.map((item) => item.value1));
          }}
        />
      </Cell>
      <div className={style.floor}>
        <div className="floorLeft">
          所在楼层 <input type="text" placeholder="请填写" /> 层
        </div>
        <div className={style.floorRight}>
          共 <input type="text" placeholder="请填写" /> 层
        </div>
      </div>
      <Cell title="面积">
        <Input
          clearable
          type="text"
          placeholder="请输入面积"
          value={title}
          onChange={(value) => {
            setTitle(value);
          }}
          onBlur={(value) => console.log(`onBlur: ${value}`)}
        />
      </Cell>
      <Cell title="售价">
        <Input
          clearable
          type="text"
          placeholder="请输入售价"
          value={title1}
          onChange={(value) => {
            setTitle1(value);
          }}
          onBlur={(value) => console.log(`onBlur: ${value}`)}
        />
      </Cell>
      <Cell
        description={
          <Checkbox.Group
            type="button"
            defaultValue={["0", "1"]}
            className={style.label}
            ghost={true}
          >
            {Household &&
              Household.map((item, index) => {
                return (
                  <Checkbox value={index} key={item.id}>
                    {item.name}
                  </Checkbox>
                );
              })}
          </Checkbox.Group>
        }
      >
        标签
      </Cell>
      <Cell title="标题" className={style.label}>
        <Input
          clearable
          type="text"
          placeholder="请输入"
          value={title5}
          onChange={(value) => {
            setTitle5(value);
          }}
          onBlur={(value) => console.log(`onBlur: ${value}`)}
        />
      </Cell>
      <Cell title="描述">
        <Input
          clearable
          type="text"
          placeholder="请输入"
          value={title3}
          onChange={(value) => {
            setTitle3(value);
          }}
          onBlur={(value) => console.log(`onBlur: ${value}`)}
        />
      </Cell>
      <Cell title="联系人">
        <Input
          clearable
          type="text"
          placeholder="请输入"
          value={title4}
          onChange={(value) => {
            setTitle4(value);
          }}
          onBlur={(value) => console.log(`onBlur: ${value}`)}
        />
      </Cell>
      <Cell
        title="有效时间"
        description={
          <Checkbox.Group
            type="button"
            defaultValue={["0", "1"]}
            className={style.label}
            ghost={true}
          >
            <Checkbox className={style.activeTime}>30天</Checkbox>
          </Checkbox.Group>
        }
      ></Cell>

      <div className={style.reading}>
        <input type="checkbox" />
        我已接受并阅读《宏烨找房网房源信息发布规则》
      </div>
      <div className={style.launch}>
        <button
          className={style.launch1}
          onClick={() => router.push("/successpay")}
        >
          发布并支付
        </button>
      </div>
    </div>
  );
};
export default TwoRental;
