import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import style from './index.module.scss'
import { FilePicker, Icon, Toast, Badge } from 'zarm';
import {useHistory} from "react-router-dom"
const MAX_FILES_COUNT = 1;

const onBeforeSelect = () => {
};

const UploadImg = () => {
    const router = useHistory();
    const [files, setFiles] = useState([]);

    const onSelect = (selFiles) => {
        const newFiles = files.concat(selFiles);
        if (newFiles.length > MAX_FILES_COUNT) {
            Toast.show('最多只能选择5张图片');
            return;
        }
        setFiles(newFiles);
    };

    const remove = (index) => {
        const newFiles = [].concat(files);
        newFiles.splice(index, 1);
        setFiles(newFiles);
        Toast.show('删除成功');
    };

    const imgRender = () => {
        return files.map((item, index) => {
            return (
                <Badge
                    key={+index}
                    className="file-picker-item"
                    shape="circle"
                    text={
                        <span className="file-picker-closebtn">
                            <Icon type="wrong" />
                        </span>
                    }
                    onClick={() => remove(index)}
                >

                    <div className="file-picker-item-img">
                        <a href={item.thumbnail} target="_blank" rel="noopener noreferrer" className={style.picker}>
                            <img src={item.thumbnail} alt="" className={style.wrapper} />
                        </a>
                    </div>
                </Badge>
            );
        });
    };


    return <div className="append">
        <div className={style.house}>
            <Link to="/index/issue" className={style.fan}>＜</Link>
            <b>添加图片</b>
            <p></p>
        </div>
        <div className={style.indoor}>
            <b>室内图/视频</b>
            <p>最多上传1张</p>
        </div>
        <div className="file-picker-wrapper">
            {imgRender()}
            {files.length < MAX_FILES_COUNT && (
                <FilePicker
                    multiple
                    className="file-picker-btn"
                    accept="image/*"
                    onBeforeSelect={onBeforeSelect}
                    onChange={onSelect}
                >
                    <Icon type="add" size="lg" className={style.add} />
                </FilePicker>
            )}
        </div>
        <div className={style.launch2}>
            <button onClick={()=>router.go(-1)} className={style.launch3}>完成</button>
        </div>
    </div>
}

export default UploadImg;