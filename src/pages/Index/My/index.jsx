import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import style from "./index.module.scss";
import { admin } from "@/api/api";
function My() {
  let history = useHistory();
  const [list, setList] = useState();
  const admins = async () => {
    const res = await admin();
    setList(res.data.body[0]);
  };

  useEffect(() => {
    admins();
  }, []);
  const message = () => {
    history.push("/message");
  };
  const myissue = () => {
    history.push("/myissue");
  };
  const question = () => {
    history.push("/feedback");
  };
  const payload = () => {
    history.push("/payment");
  };
  
  
  const browsing=()=>{
    history.push("/browsing")
  }
  const collent=()=>{
    history.push("/collent")
  }

  return (
    <div className={style.my}>
      <div className={style.heard}>
        <b>我 的</b>
      </div>
      <div className={style.float}>
        <div className={style.up}>
          <p>
            <img
              src="https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fi2.sinaimg.cn%2FIT%2F2013%2F0106%2FU6680P2DT20130106000119.jpg&refer=http%3A%2F%2Fi2.sinaimg.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1632056355&t=2b7c6f70884cf92fc501c7c012514724"
              alt=""
            />
          </p>
          <p className={style.p}>
            <b>{list && list.nickname}</b>
            <span>{list && list.phone}</span>
          </p>
          <p onClick={message}>
            <i className="iconfont icon-xinxi" style={{ color: "blue" }}></i>
          </p>
        </div>
        <div className={style.down}>
          <p onClick={myissue}>
            <i className="iconfont icon-fabu" style={{ color: "green" }}></i>
            <span>我的发布</span>
          </p>
          <p>
            <i className="iconfont icon-shoucang" style={{ color: "pink" }}></i>
            <span onClick={collent}>收藏</span>
          </p>
          <p>
            <i className="iconfont icon-map1" style={{ color: "red" }}></i>

            <span onClick={browsing}>浏览历史</span>
          </p>
        </div>
      </div>
      <div className={style.bottom}>
        <div className={style.content} style={{height:(window.innerHeight-80-130)+"px"}}>
        <ul>
            <li onClick={payload}>
              <span>支付明细</span> <i> {">"} </i>
            </li>
            <li onClick={question}>
              <span>问题反馈</span> <i> {">"} </i>
            </li>
            <li >
              <span><a href="tel:15803933114">联系客服</a></span> <i> {">"} </i>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}
export default My;
