import React from "react";
import { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import HeaderComponent from "../../../components/HeaderComponent";
import style from "./myissue.module.scss";
import { issuehouse } from "../../../api/api";
import Select from "../../../components/Select";
function Message() {
  let history = useHistory();
  const [list, setList] = useState([]);
  const selectlist = [
    {
      value: 0,
      label: "二手房出售",
    },
    {
      value: 1,
      label: "出租房",
    },
    {
      value: 2,
      label: "求购",
    },
    {
      value: 3,
      label: "求租",
    },
  ];
  const passlist = [
    {
      value: 0,
      label: "审核通过",
    },
    {
      value: 1,
      label: "审核未通过",
    },
  ];
  const getissuehouse = async () => {
    const res = await issuehouse();
    // console.log(res.data.body);
    setList(res.data.body);
  };
  const goback = () => {
    history.go(-1);
  };
  useEffect(() => {
    getissuehouse();
  }, []);
  const screen = (selectlist) => {
    console.log(selectlist);
  };

  return (
    <div className={style.myissue}>
      <HeaderComponent
        title="我的发布"
        back="block"
        goback={() => goback()}
      ></HeaderComponent>
      <div className={style.state}>
        <Select
          list={selectlist}
          width="100px"
          height="30px"
          getCity={() => screen(selectlist)}
        ></Select>

        <Select
          list={passlist}
          width="100px"
          height="30px"
          getCity={() => screen(selectlist)}
        ></Select>
      </div>
      <ul>
        {list &&
          list.map((item, index) => {
            return (
              <li key={index}>
                <div>
                  <img
                    src={"http://47.102.145.189:8009" + item.houseImg}
                    alt=""
                  />
                </div>
                <div>
                  <h4>{item.title}</h4>
                  <p>{item.desc}</p>
                  <p style={{ color: "orange" }}>￥{item.price}</p>
                </div>
              </li>
            );
          })}
      </ul>
    </div>
  );
}

export default Message;
