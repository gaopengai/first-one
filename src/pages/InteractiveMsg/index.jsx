import Header from "@/components/HeaderComponent";
import { useState, useEffect } from "react";
import style from "./index.module.scss";
import classNames from "classnames";
import { useHistory } from "react-router-dom";
import { chatFriends } from "@/api/api";
const InteractiveMsg = (props) => {
  let route = useHistory();
  const [list] = useState(["消息", "公告"]);
  const [Chatlist, setChatlist] = useState([]);
  const [ind, setInd] = useState(0);
  const [page] = useState(1);
  const [rows] = useState(20);
  const [is_black] = useState(0);
  const [keywords] = useState("");
  const _chatFriends = () => {
    chatFriends({ page, rows, is_black, keywords }).then((res) => {
      console.log(res);
      setChatlist(res.data.friends);
    });
  };
  const ChangeIndex = (index) => {
    setInd(index);
    if (index === 0) {
      _chatFriends();
    }
  };
  const goChatDetail=(id,chat_id)=>{
    route.push(
     `/chatDetail/${id}/${chat_id}`,
    )

  
  }
  useEffect(()=>{
    _chatFriends()
  },[])
  const goback = () => {
    route.go(-1);
  };
  return (
    <div className={style.InteractiveMsg_wraper}>
      <Header
        title={"消息列表"}
        back="block"
        goback={() => {
          goback();
        }}
      ></Header>
      <div className={style.top}>
        {list.map((item, index) => {
          return (
            <span
              className={classNames(
                style.tap,
                index === ind ? style.class : ""
              )}
              key={index}
              onClick={() => ChangeIndex(index)}
            >
              {item}
            </span>
          );
        })}
      </div>
      <div className={style.list}>
        {Chatlist.map((item, index) => {
          return (
            <div key={item.user_id} className={style.item} onClick={()=>{goChatDetail(item.user_id,item.chat_id)}}>
              <img className={style.user_img} src={item.headimage} alt="" />
              <div className={style.userInfo}>
                <div className={style.user_name}>{item.nickname}</div>
                <div className={style.content}>{item.chat.content}</div>
              </div>
              <div className={style.time}>{item.chat.time}</div>
            </div>
          );
        })}
      </div>
    </div>
  );
};
export default InteractiveMsg;
