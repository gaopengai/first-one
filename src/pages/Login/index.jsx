import React, { useState, useRef } from "react";
import style from "./index.module.scss";
import Header from "../../components/HeaderComponent";
import { Button, Toast } from "antd-mobile";
import { useHistory } from "react-router-dom";
import { _Login } from "@/api/api";
import imgs from "../../images/images.png";
const Login = () => {
  let router_push = useHistory();

  const [username, setuser] = useState("");
  const [password, setpaw] = useState("");

  const failToast = () => {
    Toast.fail("输入的账号或者密码异常", 1);
  };
  const handleLogin = async () => {
    let res = await _Login({ username, password });
    console.log(res);
    if (res.data.status === 200) {
      window.sessionStorage.authorization = res.data.body.token;
      router_push.push("/index/home");
    } else if (res.data.status === 400) {
      failToast();
    }
  };
  const getuser = (e) => {
    setuser(e.target.value);

    let req = /^[1][3,4,5,6,7,8][0-9]{9}$/;
    if (req.test(e.target.value)) {
      console.log(username);
    } else {
      console.log("手机号输入格式不正确");
    }
  };
  const loginref = useRef(null);

  const pwdsevent = () => {
    loginref.current.style = `margin-bottom:200px`;
    // loginref.current.scrollTop = "200px";
  };
  const pwdevent = () => {
    loginref.current.style = `margin-bottom:0px`;
  };

  return (
    <div className={style.login} ref={loginref}>
      <Header title={"宏烨经纪人"}></Header>
      <div className={style.loginWraper}>
        <img className={style.imgs} src={imgs} alt="" />
        <h3>用心服务 创造未来</h3>
        <input
          className={style.user}
          autoFocus="autofocus"
          onChange={(e) => getuser(e)}
          type="text"
          placeholder="输入用户名"
        />
        <input
          type="password"
          className={style.pwd}
          onChange={(e) => setpaw(e.target.value)}
          onFocus={pwdsevent}
          onBlur={pwdevent}
          placeholder="输入验证码"
        />
        <Button className={style.btn} type="primary" onClick={handleLogin}>
          立即登录
        </Button>
      </div>
    </div>
  );
};

export default Login;
