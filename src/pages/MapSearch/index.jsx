import { Icon } from "antd-mobile";
import style from "./mapsearch.module.scss";
import Header from "@/components/HeaderComponent";
import Amap from "@/components/Amap";
import Select from "@/components/Select";
import MapSelection from "@/components/MapSelection";
import { getcity } from "@/api/api.js";
import { useEffect, useState, useRef } from "react";
const MapSearch = (props) => {
  const typelist = [
    {
      img: "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fpic.soutu123.com%2Felement_origin_min_pic%2F17%2F02%2F04%2F9281cfde19d531e271864c132047ea9a.jpg%21%2Ffw%2F700%2Fquality%2F90%2Funsharp%2Ftrue%2Fcompress%2Ftrue&refer=http%3A%2F%2Fpic.soutu123.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1632050806&t=f292a88dd82b07efa0598ab64957e23b",
      title: "买房",
    },
    {
      img: "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimages.669pic.com%2Felement_pic%2F95%2F93%2F25%2F55%2F92545b0f4b3ad05b77377e439b0ea5c9.jpg&refer=http%3A%2F%2Fimages.669pic.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1632047546&t=9b72f3e6989bf9dbe557a8c46ba53aab",
      title: "租房",
    },
  ];
  const navlist = [
    {
      title: "区域",
      icon: "down",
    },
    {
      title: "价格",
      icon: "down",
    },
    {
      title: "户型",
      icon: "down",
    },
    {
      title: "距离",
      icon: "down",
    },
  ];
  const searchmap = useRef(null);
  const [layer, setlayer] = useState(false);
  const [val, setval] = useState("");
  const [mask, setmask] = useState(true);
  const goback = () => {
    props.history.go(-1);
  };
  const changelayer = (f) => {
    setlayer(f);
  };
  const changesearch = (e) => {
    setval(e.target.value);
  };
  // 城市
  const [citylist, setcitylist] = useState([]);
  const [flag, setflag] = useState(false);
  const [actived, setactiveed] = useState(0);
  const init = async () => {
    let res = await getcity();
    if (res.status === 200) {
      setcitylist(res.data.body);
      setflag(true);
    }
  };
  const changecity = (ite) => {
    setval(ite.label);
    searchmap.current.focus();
  };
  const changemask = () => {
    setmask(false);
  };
  const gofilrate = (e) => {
    if (
      e.target.classList.contains("区域") ||
      e.target.classList.contains("价格") ||
      e.target.classList.contains("户型")
    ) {
      props.history.push("/filrate");
    }
  };
  useEffect(() => {
    init();
  }, []);
  return (
    <>
      {flag ? (
        <div className={style.mapsearch}>
          <Header
            title="地图找房"
            back="block"
            height="90px"
            goback={goback}
          ></Header>

          <div className={style.mapnav1}>
            <div className={style.searchtit}>
              {citylist.length > 0 && (
                <Select
                  list={citylist}
                  width="80px"
                  height="30px"
                  getCity={changecity}
                ></Select>
              )}
              <div className={style.search}>
                <input
                  type="text"
                  placeholder="请在这里输入区域/商圈"
                  ref={searchmap}
                  value={val}
                  onInput={(e) => changesearch(e)}
                />
                <Icon type="search" size="xs" />
              </div>
            </div>
            <div className={style.choice}>
              {typelist.map((ite, i) => {
                return (
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      alignItems: "center",
                    }}
                    key={i}
                    onClick={() => setactiveed(i)}
                  >
                    <img src={ite.img} width="60px" height="60px" alt="" />
                    <span className={actived === i ? style.active : null}>
                      {ite.title}
                    </span>
                  </div>
                );
              })}
            </div>
          </div>
          <div onClick={(e) => gofilrate(e)} className={style.selectbox}>
            <div
              className={style.fix}
              onClick={() => {
                setlayer(false);
              }}
              style={{
                width: "100%",
                height: "100%",
                background: "rgba(0,0,0,.5)",
                position: "absolute",
                bottom: 0,
                left: 0,
                top: 0,
                right: 0,
                display: layer ? "block" : "none",
                zIndex: 20,
              }}
            ></div>
            <MapSelection
              list={navlist}
              city={citylist}
              changelayer={changelayer}
              layer={layer}
            ></MapSelection>
          </div>
          <Amap input={searchmap && searchmap} changemask={changemask}></Amap>
          <div
            className={style.layer}
            style={{ display: mask ? "flex" : "none" }}
          >
            <p>获取定位中...</p>
          </div>
        </div>
      ) : (
        <div>loading</div>
      )}
    </>
  );
};

export default MapSearch;
