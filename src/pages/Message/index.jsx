import React from "react";
// import RouterView from "@/router/RouterView";
import { useHistory } from "react-router-dom";
import HeaderComponent from "../../components/HeaderComponent";
import style from "./index.module.scss";

function Message() {
  let route = useHistory();
  const gointeractiveMsg = () => {
    route.push("/interactiveMsg");
  };

  const goback = () => {
    route.go(-1);
  };
  const goSystem = () => {
    route.push("/system");
  };
  return (
    <div className={style.message}>
      <HeaderComponent
        title="消息"
        back="block"
        goback={() => {
          goback();
        }}
      ></HeaderComponent>

      <ul>
        <li>
          <span>互动消息</span>
          <i
            className="iconfont icon-fanhui1"
            style={{ paddingLeft: "5px" }}
            onClick={gointeractiveMsg}
          ></i>
        </li>
        <li>
          <span>系统消息</span>{" "}
          <i className="iconfont icon-fanhui1" onClick={goSystem}></i>
        </li>
      </ul>
    </div>
  );
}

export default Message;
