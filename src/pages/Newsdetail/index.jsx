import {useState,useEffect} from 'react'
import {getnewsdetail} from '@/api/api'
import style from './index.module.scss'

function Newsdetail(props){
  const [list,setList]=useState([])
  const detail= async()=>{
    const res=await getnewsdetail()
    console.log(res.data);
    setList(res.data)
  }
  useEffect(() => {
    detail()
  
  }, [])
  const goback=()=>{

    props.history.go(-1)
  }
return(
  <div className={style.detail}>
     {
      <div>
        <p><span onClick={goback}>&lt;</span> <b>{list.newDetail&&list.newDetail.title}</b> </p>
      <h2>{list.newDetail&&list.newDetail.title}</h2>
      <nav>{list.newDetail&&list.newDetail.update_time} {list&&list.siteName} 来源:{list.newDetail&&list.newDetail.from} </nav>
      <nav>{list.newDetail&&list.newDetail.categoryname} </nav>
      {list.newDetail&&<li dangerouslySetInnerHTML={{ __html: list.newDetail.content }}></li>}
       <nav>{list.newDetail&&list.newDetail.declare}</nav>
        <li>最新看房活动，楼盘信息活动通知我</li>
        <i>预约优惠</i>
        <i>团购看房</i>
        <h3>更多好文：</h3>
        {
          list.newOther&&list.newOther.map((item,index)=>{
            return <div key={index} className={style.newcontent}>
             
               <div>
                 <p> {item.title}</p>
               </div>
               <img src={item.img} alt="" />
            </div>
          })
        }
      </div>
     }

  </div>
)
} 
export default Newsdetail