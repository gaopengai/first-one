import { useEffect, useState } from "react";
import style from "./index.module.scss";
import { SearchBar } from "antd-mobile";

const Online = (props) => {
  let [msgData] = useState([]);
  let [customer, setCustomer] = useState({});
  let [value, setValue] = useState("");

  useEffect(() => {
    setCustomer(props.history.location.state);
  },{});

  const changeValue = (e) => {
    setValue(e);
  };
  // 点击发送
  const handleClick = () => {
    let obj1 = {
      uid: customer.user_id,
      name: customer.nickname,
      img: customer.headimage,
      children: [
        "亲，你想了解哪方面的信息",
        "你好",
        "哈哈哈哈哈哈",
        "嘻嘻",
        "嘿嘿",
      ],
    };
    let obj2 = {
      name: "admin",
      sid: new Date() * 1,
      children: [value],
    };
    msgData.push(obj1, obj2);
    setValue("");
    console.log(msgData);
  };

  return (
    <div className={style.online}>
      <div className={style.online_header}>
        <div className={style.online_hhh}>
          <p
            onClick={() => {
              props.history.go(-1);
            }}
          >
            &lt;
          </p>
          <p>消息</p>
          <p></p>
        </div>
      </div>
      <div className={style.online_con}>
        <div className={style.online_con_left}>
          <ul>
            <li>sdfsd爱上犯得上反对大师傅dads放大fs</li>
          </ul>
        </div>
        <div className={style.online_con_right}>
          {msgData.map((item) => {
            return (
              <ul key={item.id}>
                <li>{item.tit}</li>
              </ul>
            );
          })}
        </div>
      </div>
      <div className={style.online_inp}>
        <SearchBar
          placeholder="Search"
          showCancelButton
          cancelText="发送"
          value={value}
          onChange={(e) => changeValue(e)}
          onCancel={() => handleClick()}
        />
      </div>
    </div>
  );
};

export default Online;
