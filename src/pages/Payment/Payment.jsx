import HeaderComponent from "@/components/HeaderComponent";
import { Icon } from "antd-mobile";

import "./Payment.scss";
function Payment (props)  {
  const goback=()=>{
    props.history.go(-1)
  }
  return (
    <div className='Payment'>
      <HeaderComponent
        title='支付明细'
        back='block'
        goback={() =>goback()}
      ></HeaderComponent>
      <div className='parment-list'>
        <h2>
          2021年1月 <Icon type='down' style={{ marginTop: 3 }}></Icon>
        </h2>
        <p className='p'>支出￥1917.61</p>
      </div>
      <div className='interaction-lists'>
        <div className='cyan-lists'>
          <dl>
            <dt>
              <img src="https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fp1.ssl.cdn.btime.com%2Ft0130a19ab50e993006.jpg%3Fsize%3D386x386&refer=http%3A%2F%2Fp1.ssl.cdn.btime.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1632403527&t=e9b2816c344f717aef45189fd84afff5" alt='' />
            </dt>
            <dd>
              <p>微信支付 宏烨找房</p>
              <p>1月11日 12： 16</p>
            </dd>
          </dl>
          <div className='hours'>-10:00</div>
        </div>
      </div>
      <div className='interaction-lists'>
        <div className='cyan-lists'>
          <dl>
            <dt>
              <img src="https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fp1.ssl.cdn.btime.com%2Ft0130a19ab50e993006.jpg%3Fsize%3D386x386&refer=http%3A%2F%2Fp1.ssl.cdn.btime.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1632403527&t=e9b2816c344f717aef45189fd84afff5" alt='' />
            </dt>
            <dd>
              <p>微信支付 宏烨找房</p>
              <p>1月11日 12： 16</p>
            </dd>
          </dl>
          <div className='hours'>-10:00</div>
        </div>
      </div>
    </div>
  );
};

export default Payment;
