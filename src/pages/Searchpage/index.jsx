import { useState, useEffect } from "react";
import style from "./index.module.scss";
import { SearchBar } from "antd-mobile";
import { connect } from "react-redux";
import { getListAll } from "@/store/action/action.js";

const Searchpage = (props) => {
  const [getData, setGetData] = useState([]);
  useEffect(() => {
    props.getListData();
  }, []);
  // 返回
  const goback = () => {
    props.history.go(-1);
  };
  // // 获取输入框value
  const getSearchValue = (fn) => {
    // 防抖  
    let timer = null;
    return function (e) {
      if (timer) {
        clearTimeout(timer);
      }
      timer = setTimeout(() => {
        fn(e);
      }, 1000);
    };
  };
  const aaa = (e) => {
    let listdata = props.listdataAll.filter(
      (item) => item.title.includes(e) || item.i1.includes(e)
    );
    setGetData(listdata);
    if (e === "") {
      setGetData([]);
    }
  };
  const show = getSearchValue(aaa);

  const tohotfloorDetail = (id) => {
    props.history.push(`/tohotfloordetail/${id}`);
  };

  return (
    <div className={style.search}>
      <div className={style.sss}>
        <div className={style.tit}>
          <div className={style.tit_top}>
            <p onClick={() => goback()}>&lt;</p>
            <b>搜索</b>
            <i></i>
          </div>
          <div className={style.tit_search}>
            <SearchBar
              placeholder="Search"
              onCancel={() => goback()}
              showCancelButton
              onChange={(e) => {
                show(e);
              }}
            />
          </div>
        </div>
      </div>
      <div className={style.con}>
        {getData.map((item) => {
          return (
            <dl key={item.id} onClick={() => tohotfloorDetail(item.id)}>
              <dt>
                <img src={item.img} alt="" />
              </dt>
              <dd>
                <h3>
                  {item.title}
                  <span
                    className={style.Hotproperty_con_status_name}
                    style={{ background: item.status_color }}
                  >
                    {item.status_name}
                  </span>
                </h3>
                <p className={style.Hotproperty_con_price}>
                  {item.build_price}
                  <span>元/m²</span>
                </p>
                <p className={style.Hotproperty_con_type}>{item.b_type}</p>
                <p className={style.Hotproperty_con_kfs}>{item.kfs}</p>
              </dd>
            </dl>
          );
        })}
      </div>
    </div>
  );
};
const mapStateToProps = (state) => {
  return {
    listdataAll: state.listdataAll,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    getListData() {
      dispatch(getListAll());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Searchpage);
