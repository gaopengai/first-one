import { useEffect, useState } from "react";
import { connect } from "react-redux";
import { getListAll, getcollect } from "@/store/action/action.js";
import { Steps, WingBlank } from "antd-mobile";
import style from "./index.module.scss";
// import {} from
const Step = Steps.Step;

const TohotfloorDetail = (props) => {
  const [idd, setid] = useState();
  useEffect(() => {
    props.getListdata();
    setid(props.match.params.id);
  }, []);

  let arr =
    props.listdata.length &&
    props.listdata.filter((item) => {
      console.log(idd);
      return item.id + "" === idd + "";
    });
  console.log(arr);

  // 返回
  const goback = () => {
    props.history.go(-1);
  };

  // 去Echarts
  const toEcharts = (item) => {
    props.history.push({
      pathname: "/echarts",
      state: item,
    });
  };

  return (
    <div className={style.all}>
      {arr.length &&
        arr.map((item) => {
          return (
            <div className={style.all_con} key={item.id}>
              <div className={style.all_con_tit}>
                <p onClick={() => goback()}>&lt;</p>
                <img src={item.img} alt="" />
              </div>
              <div className={style.all_con_con}>
                <div className={style.all_con_con_tit}>
                  <h3>
                    {item.title}
                    <span style={{ background: item.status_color }}>
                      {item.status_name}
                    </span>
                  </h3>
                  {item.b_type.map((itm) => {
                    return (
                      <span key={itm} className={style.all_con_con__tit_span}>
                        {itm}
                      </span>
                    );
                  })}
                </div>
                <div className={style.all_con_con_price}>
                  <div className={style.all_con_con_price_zuo}>
                    <h3>
                      {item.price}
                      <span>{item.price_unit}</span>
                    </h3>
                    <p onClick={() => toEcharts(item)}>参考均价 📝</p>
                  </div>
                  <div className={style.all_con_con_price_zhong}>
                    <h3>
                      {item.minHuxing}万<span>起</span>
                    </h3>
                    <p>参考总价</p>
                  </div>
                  <div className={style.all_con_con_price_you}>
                    <h3>
                      {item.i5}
                      <span>m²</span>
                    </h3>
                    <p>建议范围</p>
                  </div>
                </div>
                <div className={style.all_con_con_address}>
                  <i className="iconfont icon-map"></i>
                  <p>{item.address}</p>
                </div>
                <div className={style.all_con_con_bg}>
                  <img
                    src="https://images.sqfcw.com/images/new_icon/quan.png?x-oss-process=style/w_8601"
                    alt=""
                  />
                  <div className={style.all_con_con_bg_con}>
                    <p>欢迎品鉴</p>
                    <div className={style.all_con_con_bg_con_desc}>
                      <p>获取优惠</p>
                      <p>结束时间 12-30</p>
                    </div>
                  </div>
                </div>
                <div className={style.all_con_con_msg}>
                  <WingBlank size="lg">
                    <Steps size="small" current={1}>
                      <Step title="公司" description={item.kfs} />
                      <Step title="地址" description={item.sellAddress} />
                      <Step title="交通" description={item.matches} />
                    </Steps>
                  </WingBlank>
                  <h3>房子信息：</h3>
                  <p
                    style={{ color: "rgb(121, 116, 111)" }}
                    className={style.pppp}
                    dangerouslySetInnerHTML={{ __html: item.content }}
                  ></p>
                  <button onClick={props.collect(arr)} className={style.p}>
                    点击收藏 <i className="iconfont icon-shoucang"></i>
                  </button>
                </div>
              </div>
            </div>
          );
        })}
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    listdata: state.listdataAll,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    getListdata() {
      dispatch(getListAll());
    },
    collect(arr) {
      dispatch(getcollect(arr));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TohotfloorDetail);
