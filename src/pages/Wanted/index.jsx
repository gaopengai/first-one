import { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import { getwanted, gethouseCondition } from '@/api/api'
import HeaderComponent from '../../components/HeaderComponent'
import style from './index.module.scss'
function Wanted() {
  let history = useHistory()
  const [data, setDate] = useState({
    show: false,
    display: false,
    mask: false
  })
  const [list, setList] = useState([])
  const wanted = async () => {
    const res = await getwanted()
    console.log(res.data.house);
    setList(res.data.house)
  }
  const [area, setArea] = useState([])

  const condition = async () => {
    const res = await gethouseCondition()
    console.log(res.data);
    setArea(res.data)
  }
  useEffect(() => {
    wanted()
    condition()

  }, [])
  const goback = () => {
    history.go(-1)
  }
  const catname = () => {
    let { show,  mask } = data
    setDate({
      show:!show,
      display:false,
      mask:!mask
    })
  }
  const areas = () => {
    let { show, mask } = data
    setDate({
      mask:!mask,
      display:true,
      show:!show,
    })
  }
  return (
    <div className={style.wanted}>
      <HeaderComponent
        title="房屋求租"
        back="block"
        goback={() => goback()}></HeaderComponent>
      <div className={style.box}>


        <div className={style.pop}>
          <span onClick={catname}>房产类型 <i className="iconfont icon-xia1"></i></span>
          <span onClick={areas}>区域选择 <i className="iconfont icon-xia1"></i></span>
        </div>
        {
          data.show ? <ul className={style.flutter}>
            {
              data.display ? area.area && area.area.map((item, index) => {
                return <li key={index}>{item.areaname} </li>
              }) : area.cates && area.cates.map((item, index) => {
                return <li key={index}> {item.catname}</li>
              })
            }
            <div className={data.mask ? style.mask : ''} />


          </ul> : ''

        }
        <div className={style.content}>
          {
            list && list.map((item, index) => {
              return <div className={style.list} key={index}>
                <div className={style.lis}>
                  <div className={style.images}>
                    <img src={item.prelogo} alt="" />

                    <b>{item.cname} </b>
                  </div>
                  <span> {item.begintime}</span>
                </div>
                <p>{item.title}</p>
                 <div>
                   <span>意向区域：{item.areaname}</span>
                   <span>意向价格：<b>￥{item.qzprice}</b> </span>
                   <i className="iconfont icon-shoucang"></i>
                   <i className="iconfont icon-xinxi"></i>
                  
                 </div>
              </div>
            })
          }
        </div>


      </div>
    </div>
  )
}
export default Wanted