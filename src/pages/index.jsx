import React, { useState, useEffect, useRef } from "react";
import "./index.scss";
import RouterView from "@/router/RouterView";
import { NavLink } from "react-router-dom";

const FirstPage = (props) => {
  const [navlist] = useState([
    {
      name: "首页",
      icon: "iconfont icon-shouye",
      path: "/index/home",
    },
    {
      name: "找房",
      icon: "iconfont icon-chazhaofangyuan",
      path: "/index/findhouse",
    },
    {
      name: "发布",
      icon: "iconfont icon-fabu",
      path: "/index/issue",
    },
    {
      name: "消息",
      icon: "iconfont icon-xiaoxi",
      path: "/message",
    },
    {
      name: "我的",
      icon: "iconfont icon-wode",
      path: "/index/my",
    },
  ]);
  const foot = useRef(null);
  useEffect(() => {
    //监听页面尺寸变化
    window.addEventListener("resize", function () {
      // 解决键盘弹起后遮挡输入框的问题
      if (
        document.activeElement.tagName === "INPUT" ||
        document.activeElement.tagName === "TEXTAREA"
      ) {
        window.setTimeout(function () {
          document.activeElement.scrollIntoViewIfNeeded();
        }, 0);
      }
      // 解决键盘弹起把固定在底部的按钮顶上去的问题
      if (document.body.scrollHeight < 500 && foot.current) {
        foot.current.style.display = "none";
      } else if (document.body.scrollHeight >= 500 && foot.current) {
        foot.current.style.display = "flex";
      }
    });
  }, []);
  //     // 解决键盘弹起把固定在底部的按钮顶上去的问题
  //     // btnSubmit是按钮区域的的节点id
  //     // const bottomBtn = document.querySelector("#btnSubmit");
  //     if (document.body.scrollHeight < 500) {
  //       foot.current.style.display = "none";
  //     } else {
  //       foot.current.style.display = "flex";
  //     }
  //   })
  // }, [])
  return (
    <div className="all">
      <div className="main">
        <RouterView routes={props.routes}></RouterView>
      </div>

      <div className="footer" ref={foot}>
        {navlist.map((item, index) => {
          return (
            <NavLink to={item.path} key={index}>
              <i className={item.icon}></i>
              <p>{item.name}</p>
            </NavLink>
          );
        })}
      </div>
    </div>
  );
};

export default FirstPage;
