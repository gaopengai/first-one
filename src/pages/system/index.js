import Header from "@/components/HeaderComponent";
import {useHistory} from "react-router-dom"
import style from "./index.module.scss"
const System = () => {
    const route=useHistory()
    const goback=()=>{
        route.push("/message")
        
    }
  return (
    <div className={style.system}>
      <Header
        title="消息"
        back="block"
        goback={() => {
          goback();
        }}
      ></Header>
      
    </div>
  );
};
export default System;
