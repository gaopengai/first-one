import { BrowserRouter, HashRouter } from "react-router-dom";
import RouterView from "./RouterView";

const Router = ({ config }) => {
  // 容错处理，
  let { mode, routes } = Object.assign(
    {},
    { mode: "hash", routes: [] },
    config
  );
  // 判断是什么模式
  return mode === "hash" ? (
    <HashRouter>
      <RouterView routes={routes}></RouterView>
    </HashRouter>
  ) : (
    <BrowserRouter>
      <RouterView routes={routes}></RouterView>
    </BrowserRouter>
  );
};

export default Router;
