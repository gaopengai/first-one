import { Switch, Route, Redirect } from "react-router-dom";
import { Suspense } from "react";

export const RouterView = ({ routes }) => {
  return (
    <Suspense fallback={<h1>loading...</h1>}>
      <Switch>
        {routes.map((item) => {
          return item.redirect ? (
            <Redirect
              key={item.from}
              from={item.from}
              to={item.redirect}
              exact
            ></Redirect>
          ) : (
            <Route
              key={item.name}
              path={item.path}
              render={(RouterConfig) => {
                document.title = item?.meta?.title ?? "宏烨找房";
                let Comp = item.component;
                return item.isLogin ? (
                  sessionStorage.getItem("authorization") ? (
                    <Comp {...RouterConfig} routes={item.routes || []}></Comp>
                  ) : (
                    <Redirect from={item.path} to="/login" exact></Redirect>
                  )
                ) : (
                  <Comp {...RouterConfig} routes={item.routes || []}></Comp>
                );
              }}
            ></Route>
          );
        })}
      </Switch>
    </Suspense>
  );
};

export default RouterView;
