import { lazy } from "react";

const config = {
  mode: "history",
  routes: [
    {
      from: "/",
      redirect: "/index",
    },
    {
      path: "/index",
      name: "Index",
      isLogin: true,
      component: lazy(() => import("../pages/index.jsx")),
      routes: [
        {
          from: "/index",
          redirect: "/index/home",
        },
        // 首页
        {
          path: "/index/home",
          name: "Home",
          meta: { title: "首页" },
          component: lazy(() => import("../pages/Index/Home")),
        },
        // 找房
        {
          path: "/index/findhouse",
          name: "Findhouse",
          meta: { title: "找房" },
          component: lazy(() => import("../pages/Index/Findhouse")),
        },
        // 发布
        {
          path: "/index/issue",
          name: "Issue",
          meta: { title: "发布" },
          component: lazy(() => import("../pages/Index/Issue")),
        },
        // 我的
        {
          path: "/index/my",
          name: "My",
          meta: { title: "我的" },
          component: lazy(() => import("../pages/Index/My")),
        },
      ],
    },
    // 搜索页面
    {
      path: "/searchpage",
      name: "searchpage",
      meta: { title: "首页搜索" },
      component: lazy(() => import("../pages/Searchpage")),
    },
    // 热门楼盘详情
    {
      path: "/tohotfloordetail/:id",
      name: "tohotfloordetail",
      meta: { title: "热门楼盘详情" },
      component: lazy(() => import("../pages/TohotfloorDetail")),
    },
    // Echarts
    {
      path: "/echarts",
      name: "echarts",
      meta: { title: "Echarts" },
      component: lazy(() => import("../pages/Echarts")),
    },
    // 地图找房
    {
      path: "/mapsearch",
      name: "mapsearch",
      meta: { title: "地图找房" },
      component: lazy(() => import("../pages/MapSearch/index.jsx")),
    },
    // 二手房
    {
      path: "/anyang",
      name: "anyang",
      meta: { title: "二手房" },
      component: lazy(() => import("../pages/Anyang")),
    },
    // 消息
    {
      path: "/message",
      name: "Message",
      meta: { title: "消息" },
      component: lazy(() => import("../pages/Message/index.jsx")),
    },
    {
      path: "/filrate",
      name: "filrate",
      component: lazy(() => import("../pages/Filrate/index.jsx")),
    },
    {
      path: "/login",
      name: "Login",
      meta: { title: "登录" },
      component: lazy(() => import("../pages/Login")),
    },
    //二手房出售 页面
    {
      path: "/tworental",
      name: "ershoufang",
      meta: { title: "二手房出售" },
      component: lazy(() => import("../pages/Index/Issue/tworental")),
    },
    //有房出租 页面
    {
      path: "/rental",
      name: "rentalhouse",
      meta: { title: "有房出租" },
      component: lazy(() => import("../pages/Index/Issue/rental")),
    },
    //我想买个房子 页面
    {
      path: "/wantbuy",
      name: "wantbuy",
      meta: { title: "买房" },
      component: lazy(() => import("../pages/Index/Issue/wantbuy")),
    },
    //我想租个房子 页面
    {
      path: "/wantrental",
      name: "wantrental",
      meta: { title: "租房" },
      component: lazy(() => import("../pages/Index/Issue/wantrental")),
    },
    //成功支付 页面
    {
      path: "/successpay",
      name: "successpay",
      meta: { title: "成功支付" },
      component: lazy(() => import("../pages/Index/Issue/successpay")),
    },
    //买房
    {
      path: "/buyhouse",
      name: "buyhouse",
      meta: { title: "买房" },
      component: lazy(() => import("../pages/BuyHouse/index.jsx")),
    },
    //问题反馈
    {
      path: "/feedback",
      name: "feedback",
      meta: { title: "问题反馈" },
      component: lazy(() => import("../pages/Feedback")),
    },
    //搜索小区
    {
      path: "/selectcommunity",
      name: "selectcommunity",
      meta: { title: "搜索小区" },
      component: lazy(() => import("../pages/Index/Issue/selectCommunity")),
    },
    //上传图片
    {
      path: "/uploadimg",
      name: "uploadimg",
      meta: { title: "上传图片" },
      component: lazy(() => import("../pages/Index/Issue/uploadImg")),
    },
    // 我的发布
    {
      path: "/myissue",
      name: "myissue",
      meta: { title: "我的发布" },
      component: lazy(() => import("../pages/Index/Myissue")),
    },
    //消息
    {
      path: "/interactiveMsg",
      name: "interactiveMsg",
      meta: { title: "消息" },
      component: lazy(() => import("../pages/InteractiveMsg")),
    },
    // 经纪人
    {
      path: "/broker",
      name: "broker",
      meta: { title: "经纪人" },
      component: lazy(() => import("../pages/Borker")),
    },
    // 经纪人详情
    {
      path: "/detail/:id",
      name: "Detail",
      meta: { title: "经纪人详情" },
      component: lazy(() => import("../pages/Detail")),
    },
    //楼市圈
    {
      path: "/floor",
      name: "floor",
      meta: { title: "楼市圈" },
      component: lazy(() => import("../pages/Floor")),
    },
    //聊天详情
    {
      path:"/chatDetail/:id/:chatid",
      name:"chatdetail",
      meta: { title: "聊天详情" },
      component:lazy(()=>import("../pages/Chatdetail"))
    },
    {
      path: "/system",
      name: "system",
      component: lazy(() => import("../pages/system")),
    },
    {
      path: "/myissue",
      name: "myissue",
      meta: { title: "我的发布" },
      component: lazy(() => import("../pages/Index/Myissue")),
    },
    //我的咨询
    {
      path: "/consult",
      name: "consult",
      meta: { title: "我的咨询" },
      component: lazy(() => import("../pages/Consult")),
    },
    // 求租
    {
      path: "/wanted",
      name: "wanted",
      meta: { title: "求租" },
      component: lazy(() => import("../pages/Wanted")),
    },
    // 联系客服
    {
      path: "/service",
      name: "service",
      meta: { title: "联系客服" },
      component: lazy(() => import("../pages/Service/Service")),
    },
    // 消息对话框
    {
      path: "/online",
      name: "online",
      meta: { title: "消息" },
      component: lazy(() => import("../pages/Online")),
    },
    //支付页面
    {
      path: "/payment",
      name: "payment",
      meta: { title: "支付页面" },
      component: lazy(() => import("../pages/Payment/Payment")),
    },
    //历史记录
    {
      path: "/browsing",
      name: "browsing",
      meta: { title: "历史记录" },
      component: lazy(() => import("../pages/Browsing/index")),
    },
    //收藏
    {
      path: "/collent",
      name: "collent",
      meta: { title: "收藏" },
      component: lazy(() => import("../pages/Collent/index")),
    },
    
  ],
};
export default config;
