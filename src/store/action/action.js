/*
 * @Author: gp
 * @Date: 2021-08-23 14:59:48
 * @Last Modified by: mikey.zhaopeng
 * @Last Modified time: 2021-08-26 14:44:57
 */

import { getsearch, geterdata } from "@/api/api";
import { GET_LIST_ALL, GET_ER_LIST, GETHOT, GETARR } from "../type";
// 热门楼盘总数居
// 总数居
export function getListAll() {
  return async function (dispatch) {
    let res = await getsearch();
    dispatch({
      type: GET_LIST_ALL,
      payload: res.data.list,
    });
  };
}
// 二手房总数居
export function getErAll() {
  return async function (dispatch) {
    let res = await geterdata();
    dispatch({
      type: GET_ER_LIST,
      payload: res.data.list,
    });
  };
}
let itemdata = [];
export function tohotaction(item) {
  itemdata.push(item);
  console.log(itemdata, "/////////////////");
  return (dispatch) => {
    dispatch({
      type: GETHOT,
      payload: itemdata,
    });
  };
}
let arrdata = [];
export function getcollect(arr) {
  arrdata.push(...arr);
  return (dispatch) => {
    dispatch({
      type: GETARR,
      payload: arrdata,
    });
  };
}
