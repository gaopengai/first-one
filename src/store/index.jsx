import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import logger from "redux-logger";

import { GET_LIST_ALL, GET_ER_LIST, GETHOT, GETARR } from "./type";

let defaultState = {
  listdataAll: [],
  erlistdata: [],
  historydata: [],
  collentdata: [],
};
let reducer = (preState = defaultState, action) => {
  let { type, payload } = action;

  switch (type) {
    case GET_LIST_ALL:
      return {
        ...preState,
        listdataAll: [...payload],
      };
    case GET_ER_LIST:
      return {
        ...preState,
        erlistdata: [...payload],
      };
    case GETHOT:
      return {
        ...preState,
        historydata: [...payload],
      };
    case GETARR:
      return {
        ...preState,
        collentdata: [...payload],
      };

    default:
      break;
  }
  return preState;
};

export default createStore(reducer, applyMiddleware(thunk, logger));
