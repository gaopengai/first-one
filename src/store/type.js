/*
 * @Author: gp
 * @Date: 2021-08-23 14:57:10
 * @Last Modified by: mikey.zhaopeng
 * @Last Modified time: 2021-08-26 14:40:35
 */

// 热门楼盘总数据
export const GET_LIST_ALL = "GET_LIST_ALL";
// 二手房数据
export const GET_ER_LIST = "GET_ER_LIST";
export const GETHOT = "GETHOT";
export const GETARR = "GETARR";
