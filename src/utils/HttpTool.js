import axios from "axios";

const HttpAxios = axios.create({
  baseURL:
    process.env.NODE_ENV === "production" ? "http://47.102.145.189:8009/" : "", // api的base_url
  withCredentials: true, // 选项表明了是否是跨域请求
});

// 添加请求拦截器
HttpAxios.interceptors.request.use(
  function (config) {
    // 在发送请求之前做些什么
    return {
      ...config,
      headers: {
        //请求携带token
        ...config.headers,
        authorization:sessionStorage.getItem("authorization"),
      },
    };
  },
  function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
  }
);

// 添加响应拦截器
HttpAxios.interceptors.response.use(
  function (response) {
    // 对响应数据做点什么
    return response;
  },
  function (error) {
    // 对响应错误做点什么
    console.log(error.response);
    switch (error.response.status) {
      case 400:
        console.log("错误请求");
        break;
      case 401:
        console.log("未登录,请重新登录");
        break;
      case 403:
        console.log("拒绝访问");
        break;
      case 404:
        console.log("请求错误，未找到该资源");
        break;
      case 405:
        console.log("请求方法未允许");
        break;
      case 408:
        console.log("请求超时");
        break;
      case 500:
        console.log("服务器出错");
        break;
      case 503:
        console.log("服务不可用");
        break;
      case 504:
        console.log("网络超时");
        break;
      default:
        break;
    }
    return Promise.reject(error);
  }
);

export default HttpAxios;
