const homeListdata = [
  {
    name: "新房",
    url: "https://images.sqfcw.com/images/icon/icon3/ic_xinfang@3x.png?x-oss-process=style/w_240",
    path: "/buyhouse",
  },
  {
    name: "二手房",
    url: "https://images.sqfcw.com/images/icon/icon3/ic_ershoufang@3x.png?x-oss-process=style/w_240",
    path: "/anyang",
  },
  {
    name: "租房",
    url: "https://images.sqfcw.com/images/icon/icon3/ic_chuzu@3x.png?x-oss-process=style/w_240",
    path: "/wanted",
  },
  {
    name: "团购看房",
    url: "https://images.sqfcw.com/images/icon/icon3/ic_kanfangtuan@3x.png?x-oss-process=style/w_240",
  },
  {
    name: "地图找房",
    url: "https://images.sqfcw.com/images/icon/icon3/ic_map@3x.png?x-oss-process=style/w_240",
    path: "/mapsearch",
  },
  {
    name: "咨询",
    url: "https://images.sqfcw.com/images/icon/icon3/ic_zixun@3x.png?x-oss-process=style/w_240",
    path: "/consult",
  },
  {
    name: "楼市圈",
    url: "	https://images.sqfcw.com/images/icon/icon3/ic_loushiquan@3x.png?x-oss-process=style/w_240",
    path: "/floor",
  },
  {
    name: "置业顾问",
    url: "https://images.sqfcw.com/images/icon/icon3/ic_xinfang@3x.png?x-oss-process=style/w_240",
  },
  {
    name: "经纪人",
    url: "https://images.sqfcw.com/images/icon/icon3/ic_jingjiren@3x.png?x-oss-process=style/w_240",
    path: "/broker",
  },
  {
    name: "预售查询",
    url: "https://images.sqfcw.com/images/icon/icon3/ic_yushouzheng@3x.png?x-oss-process=style/w_240",
  },
];

export default homeListdata;
