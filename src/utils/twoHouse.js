const twoHandData = [
    {
        label:"小区",
        ph:"请选择小区",
        id:1,
        value:"",
    },

    {
        label:"房产类型",
        ph:"请选择",
        id:2,
        value:"",
    },
    {
        label:"区域",
        ph:"请选择",
        id:3,
        value:"",
    },
    {
        label:"户型|朝向|装修",
        ph:"请选择",
        id:4,
        value:"",
    },
    {
        label:"面积",
        ph:"请输入",
        id:5,
        value:"",
    },
    {
        label:"售价",
        ph:"请输入",
        id:6,
        value:"",
    },
    {
        label:"标题",
        ph:"请输入",
        id:7,
        value:"",
    },
    {
        label:"描述",
        ph:"请输入",
        id:8,
        value:"",
    },
    {
        label:"联系人",
        ph:"请输入",
        id:9,
        value:"",
    },

];


export default twoHandData;